package java_filess;


public class all_hehe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		If_else obj = new If_else();
		System.out.println(obj.age);
		
		//Operators
		//unary
		System.out.println("");
		System.out.println("********************UNARY********************");
		System.out.println("");
		int a=0,b=5;
		System.out.println(a++);//0
		System.out.println(a);//1
		System.out.println(a--);//1
		System.out.println(a);//0
		
		System.out.println(b);//5
		System.out.println(--b);//4
		System.out.println(b);//4
		System.out.println(++b);//5
		System.out.println(b);//5
		
		System.out.println(~a);
		System.out.println(~1);
		
		//Arithematic
		System.out.println("");
		System.out.println("********************Arthematic********************");
		System.out.println("");
		
		int c = 4;
		int d = 7;
		
		System.out.println(c+d);
		System.out.println(c-d);
		System.out.println(c*d);
		System.out.println(c/d);
		System.out.println(d%c);
		
		//Relational, logical and if-else-if
		System.out.println("");
		System.out.println("********************Relational, logical and If-else-if********************");
		System.out.println("");
		int x=10;
		int y=52;
		int z=42;
		
		if(x==y && x==z) {
			System.out.println("x, y, z are equal");
		}
		else if(x==y || x==z) {
			System.out.println("x is either equal to y or z");
		}
		else if(x>y) {
			if(x>z) {
				System.out.println("x is greatest");
			}
			else {
				System.out.println("z is greatest");
			}
		}
		else if(y>z) {
			System.out.println("y is greatest");
		}
		else {
			System.out.println("z is greatest");
		}
		
	}
}
