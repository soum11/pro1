package java_filess;

public class ovrld {
	
	public void add(int x) {
		System.out.println("Single arg");
	}
	
	public void add(double x) {
		System.out.println("type of the args differ");
	}
	
	public void add(int x, float y){
		System.out.println(y);
		System.out.println("no.. of the args differ");
	}
	
	public void add(float x, int y){
		System.out.println("order of the args differ");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ovrld obj = new ovrld();
		
		obj.add(565f);
		obj.add(52);
		obj.add(3, 10f);
		obj.add(2f, 3);

	}

}
