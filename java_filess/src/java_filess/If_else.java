package java_filess;

public class If_else {

	int age =5;

	public static void main(String[] args) {
		int age, ht, wt;
		age = 21;
		ht = 170;
		wt = 55;
		
		if(age>=21 && age<=25) {
			if(ht >= 170) {
				if(wt>=50 && wt<=65) {
					System.out.println("Eligible");
				}
				else {
					System.out.println("Weight not matching");
				}
			}
			else {
				System.out.println("height is not matching");
			}
		}
		else {
			System.out.println("age is not matching");
		}
		
		System.out.println("***********************************************************");
		int x = 5;
		if(x%2 == 0) {
			System.out.println("EVEN");
		}
		else {
			System.out.println("ODD");
		}
	}

}
