package java_filess;

public class Exp4 {
	   int a=10;
	   static int b=9;
	   int c=a*b;//all global variables are non static memebers, hence cant be used in static methods
	   
	   public void disp()
	   {
	      System.out.println(a);
	      System.out.println(b);
	      System.out.println(c);
	      
	      int j = 9;
	      char i='A';
	      System.out.println(j);
	      System.out.println(i);
	   }
	   
	   public static void main(String[] args)
	   {
	     Exp4 obj = new Exp4();
	     obj.disp();
	     System.out.println(b);	   }
}
