package java_filess;

public class method {
	
	//type1 : method taking the arguments and returning the values
	public int[] method1(int x, int y) {
		System.out.println("Type 1 method");
		int[] ret = new int[2];
		ret[0]=x;
		ret[1]=y;
		return ret;
	}
	
	//type2 : method not taking the arguments and not returning the values
	public void method2() {
		System.out.println("Type 2 method");
	}
	
	//type3 : method taking the arguments and not returning the values
	public void method3(String str) {
		System.out.println("Type 3 method");
	}
	
	//type4 : method not taking the arguments and returning the values
	public String method4() {
		System.out.println("Type 4 method");
		return "hello";
	}
	
	public static void main(String[] args){
		method obj = new method();
		
		int[] a = obj.method1(10, 20);
		System.out.println(a[0]);
		System.out.println(obj.method1(10, 20));
		obj.method2();
		obj.method3("Java");
		String s;
		s = obj.method4();
		System.out.println(s);
	}

}
