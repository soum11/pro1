package java_filess;

public class Exp3 {
	  public void disp(int a)
	  {
	    for(int i = 0; i<=10; i++)
	    {
	       System.out.println("7*"+i+" = "+a*i);
	    }
	  }
	  public static void main(String[] args)
	  {
	    int x = 7;
	    Exp3 obj = new Exp3();
	    obj.disp(x);
	  }
}
