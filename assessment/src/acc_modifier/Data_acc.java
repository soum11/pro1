package acc_modifier;


public class Data_acc {
	private int x = 100;
	int y = 30;
	protected int z = 44;
	public int a = 52;
	
	public void show(){
		System.out.println("Inside Data_acc");
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
		System.out.println(a);
	}
}

class Value extends Data_acc{
	public void disp() {
		System.out.println("Inside Value");
		//ystem.out.println(x);
		System.out.println(y);
		System.out.println(z);
		System.out.println(a);
	}
	public static void main(String[] args) {
		Value obj = new Value();
		obj.disp();
	}
}