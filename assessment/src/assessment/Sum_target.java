package assessment;

import java.util.Scanner;

public class Sum_target {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of elemnts in an array : ");
		
		int n = sc.nextInt();
		
		System.out.println("Enter the numbers now : ");
		int[] arr = new int[n];
		for(int i=0; i<n; i++) {
			arr[i] = sc.nextInt();
		}
		
		System.out.print("Enter the target number now : ");
		int target = sc.nextInt();
		
		String a,b;
		a = null;
		b = null;
		
		for(int i=0; i<n-1; i++) {
			for(int j=1; j<n; j++) {
				if(arr[i]+arr[j] == target) {
					a = String.valueOf(i);
					b = String.valueOf(j);
					break;
				}
			}
			if(a != null && b != null) {
				break;
			}
		}
		
		if(a == null && b == null) {
			System.out.println("The target "+target+" sum does not exist in array.");
		}
		else {
			System.out.println("The indices of result sum : ["+a+" , "+b+"]");
		}
		
	}

}
