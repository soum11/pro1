package assessment;

import java.util.Scanner;

class data{
	int roll_no;
	String name;
	String year;
	String sem;
	float score;
	String clg;
	String city;
	
	public void disp(data obj) {
		System.out.println("********************Student Details********************");
		System.out.println("Roll number : "+obj.roll_no);
		System.out.println("Full name : "+obj.name);
		System.out.println("Year : "+obj.year);
		System.out.println("Semester : "+obj.sem);
		System.out.println("Score : "+obj.score);
		System.out.println("College Name : "+obj.clg);
		System.out.println("City : "+obj.city);
		
	}
}

public class Student {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		data obj = new data();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("********************Enter the Student Details********************");
		System.out.print("Enter Roll number : ");
		obj.roll_no = sc.nextInt();
		
		System.out.print("Enter Full name : ");
		obj.name = sc.next();
		
		System.out.print("Enter Year : ");
		obj.year = sc.next();
		
		System.out.print("Enter Semester : ");
		obj.sem = sc.next();
		
		System.out.print("Enter Score : ");
		obj.score = sc.nextFloat();
		
		System.out.print("Enter College Name : ");
		obj.clg = sc.next();
		
		System.out.print("Enter City : ");
		obj.city = sc.next();
		
		obj.disp(obj);
		
		
	}

}
