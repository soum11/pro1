package assessment;

class SingleTn{
	static SingleTn obj = null;
	
	private SingleTn() {
		System.out.println("Inside default");
		//private constructor can be called within class only
	}
	
	public static SingleTn getObj() {
		if(obj == null) {
			obj = new SingleTn();// bcz of this default constructor is being called
		}
		return obj;
	}
}

public class SingleTn_example {

	public static void main(String[] args) {
		// TODO Auto-generated constructor stub
		SingleTn obj1 = SingleTn.getObj();
		
		SingleTn obj2 = SingleTn.getObj();
		
		System.out.println(obj1.hashCode());
		System.out.println(obj2.hashCode());
	}

}