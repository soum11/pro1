package assessment;

import java.util.Scanner;
import java.util.*;

public class Parenthesis_check {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the String : ");
		
		String str_1 = sc.nextLine();
		
		int a = 0, b = 0, c = 0;
		
		char[] new_str = str_1.toCharArray();
		
		for(int i=0; i<str_1.length(); i++) {
			if(new_str[i] == '(') {
				a++;
			}
			if(new_str[i] == ')'){
				a--;
			}
			if(new_str[i] == '[') {
				b++;
			}
			if(new_str[i] == ']'){
				b--;
			}
			if(new_str[i] == '{') {
				c++;
			}
			if(new_str[i] == '}'){
				c--;
			}
		}
		
		if(a == 0 && b == 0 && c == 0) {
			System.out.println("The given String is balanced.");
		}
		else {
			System.out.println("The given String is not balanced.");
		}
		
	}
}
