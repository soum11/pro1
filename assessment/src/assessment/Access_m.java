package assessment;

import acc_modifier.Data_acc;

class Exp2 {
	Data_acc obj = new Data_acc();
	public void disp() {
		System.out.println("Inside Exp2 disp");
		//System.out.println(obj.x);//private
		//System.out.println(obj.y);//default
		//System.out.println(obj.z);//protected
		System.out.println(obj.a);//public
	}
}

public class Access_m  extends Data_acc{
	public void disp() {
		System.out.println("Inside Access_m disp");
		//System.out.println(x);//private
		//System.out.println(y);//default
		System.out.println(z);//protected
		System.out.println(a);//public
	}
	public static void main(String[] args) {
		System.out.println("Inside Access_m main");
		Exp2 obj = new Exp2();
		obj.disp();
		
		Data_acc obj2 = new Data_acc();
		obj2.show();
		
		Access_m obj_3 = new Access_m();
		obj_3.disp();
		
	}
}