package assessment;
import java.util.Scanner;

public class Rotate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the  number of input : ");
		
		int n = sc.nextInt();
		
		int[] arr = new int[n];
		
		for(int i=0; i<n ; i++) {
			arr[i] = sc.nextInt();
		}
		
		System.out.println("Rotating "+n+" times, output will be ashown as, rotation of an array"
				+ "from shifting 1 array to n times ");
		System.out.println("");
		
		int a = n;
		int b;
		int m =1;
		int[] new_arr = new int[n];
		while(a!=0) {
			for(int i=0; i<n; i++) {
				if(m == n) {
					m = 0;
				}
				b = m;
				for(int j=0; j<n ; j++) {
					if(b == n) {
						b = 0;
					}
					new_arr[j] = arr[b];
					b++;
				}
				m++;
				for(int k=0; k<n; k++) {
					System.out.print(new_arr[k]+" ");
				}
				System.out.println(" ");
				a = a-1;
			}
		}

	}

}
