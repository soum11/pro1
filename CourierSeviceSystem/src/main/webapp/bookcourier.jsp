<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="services.*" %>
    <%@ page import="java.time.LocalDate" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book Courier</title>
<link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
<link rel="stylesheet" href="vendor/nouislider/nouislider.min.css">

<meta name="robots" content="noindex, follow">
<style type="text/css">
/* @extend display-flex; */
display-flex, .signup-content, .form-row, .label-flex, .form-radio-group {
  display: flex;
  display: -webkit-flex; }

/* @extend list-type-ulli; */
list-type-ulli, ul {
  list-style-type: none;
  margin: 0;
  padding: 0; }

/* poppins-300 - latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 300;
  src: url("../fonts/poppins/poppins-v5-latin-300.eot");
  /* IE9 Compat Modes */
  src: local("Poppins Light"), local("Poppins-Light"), url("../fonts/poppins/poppins-v5-latin-300.eot?#iefix") format("embedded-opentype"), url("../fonts/poppins/poppins-v5-latin-300.woff2") format("woff2"), url("../fonts/poppins/poppins-v5-latin-300.woff") format("woff"), url("../fonts/poppins/poppins-v5-latin-300.ttf") format("truetype"), url("../fonts/poppins/poppins-v5-latin-300.svg#Poppins") format("svg");
  /* Legacy iOS */ }
/* poppins-300italic - latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 300;
  src: url("../fonts/poppins/poppins-v5-latin-300italic.eot");
  /* IE9 Compat Modes */
  src: local("Poppins Light Italic"), local("Poppins-LightItalic"), url("../fonts/poppins/poppins-v5-latin-300italic.eot?#iefix") format("embedded-opentype"), url("../fonts/poppins/poppins-v5-latin-300italic.woff2") format("woff2"), url("../fonts/poppins/poppins-v5-latin-300italic.woff") format("woff"), url("../fonts/poppins/poppins-v5-latin-300italic.ttf") format("truetype"), url("../fonts/poppins/poppins-v5-latin-300italic.svg#Poppins") format("svg");
  /* Legacy iOS */ }
/* poppins-regular - latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 400;
  src: url("../fonts/poppins/poppins-v5-latin-regular.eot");
  /* IE9 Compat Modes */
  src: local("Poppins Regular"), local("Poppins-Regular"), url("../fonts/poppins/poppins-v5-latin-regular.eot?#iefix") format("embedded-opentype"), url("../fonts/poppins/poppins-v5-latin-regular.woff2") format("woff2"), url("../fonts/poppins/poppins-v5-latin-regular.woff") format("woff"), url("../fonts/poppins/poppins-v5-latin-regular.ttf") format("truetype"), url("../fonts/poppins/poppins-v5-latin-regular.svg#Poppins") format("svg");
  /* Legacy iOS */ }
/* poppins-italic - latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 400;
  src: url("../fonts/poppins/poppins-v5-latin-italic.eot");
  /* IE9 Compat Modes */
  src: local("Poppins Italic"), local("Poppins-Italic"), url("../fonts/poppins/poppins-v5-latin-italic.eot?#iefix") format("embedded-opentype"), url("../fonts/poppins/poppins-v5-latin-italic.woff2") format("woff2"), url("../fonts/poppins/poppins-v5-latin-italic.woff") format("woff"), url("../fonts/poppins/poppins-v5-latin-italic.ttf") format("truetype"), url("../fonts/poppins/poppins-v5-latin-italic.svg#Poppins") format("svg");
  /* Legacy iOS */ }
/* poppins-500 - latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 500;
  src: url("../fonts/poppins/poppins-v5-latin-500.eot");
  /* IE9 Compat Modes */
  src: local("Poppins Medium"), local("Poppins-Medium"), url("../fonts/poppins/poppins-v5-latin-500.eot?#iefix") format("embedded-opentype"), url("../fonts/poppins/poppins-v5-latin-500.woff2") format("woff2"), url("../fonts/poppins/poppins-v5-latin-500.woff") format("woff"), url("../fonts/poppins/poppins-v5-latin-500.ttf") format("truetype"), url("../fonts/poppins/poppins-v5-latin-500.svg#Poppins") format("svg");
  /* Legacy iOS */ }
/* poppins-500italic - latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 500;
  src: url("../fonts/poppins/poppins-v5-latin-500italic.eot");
  /* IE9 Compat Modes */
  src: local("Poppins Medium Italic"), local("Poppins-MediumItalic"), url("../fonts/poppins/poppins-v5-latin-500italic.eot?#iefix") format("embedded-opentype"), url("../fonts/poppins/poppins-v5-latin-500italic.woff2") format("woff2"), url("../fonts/poppins/poppins-v5-latin-500italic.woff") format("woff"), url("../fonts/poppins/poppins-v5-latin-500italic.ttf") format("truetype"), url("../fonts/poppins/poppins-v5-latin-500italic.svg#Poppins") format("svg");
  /* Legacy iOS */ }
/* poppins-600 - latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 600;
  src: url("../fonts/poppins/poppins-v5-latin-600.eot");
  /* IE9 Compat Modes */
  src: local("Poppins SemiBold"), local("Poppins-SemiBold"), url("../fonts/poppins/poppins-v5-latin-600.eot?#iefix") format("embedded-opentype"), url("../fonts/poppins/poppins-v5-latin-600.woff2") format("woff2"), url("../fonts/poppins/poppins-v5-latin-600.woff") format("woff"), url("../fonts/poppins/poppins-v5-latin-600.ttf") format("truetype"), url("../fonts/poppins/poppins-v5-latin-600.svg#Poppins") format("svg");
  /* Legacy iOS */ }
/* poppins-700 - latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 700;
  src: url("../fonts/poppins/poppins-v5-latin-700.eot");
  /* IE9 Compat Modes */
  src: local("Poppins Bold"), local("Poppins-Bold"), url("../fonts/poppins/poppins-v5-latin-700.eot?#iefix") format("embedded-opentype"), url("../fonts/poppins/poppins-v5-latin-700.woff2") format("woff2"), url("../fonts/poppins/poppins-v5-latin-700.woff") format("woff"), url("../fonts/poppins/poppins-v5-latin-700.ttf") format("truetype"), url("../fonts/poppins/poppins-v5-latin-700.svg#Poppins") format("svg");
  /* Legacy iOS */ }
/* poppins-700italic - latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 700;
  src: url("../fonts/poppins/poppins-v5-latin-700italic.eot");
  /* IE9 Compat Modes */
  src: local("Poppins Bold Italic"), local("Poppins-BoldItalic"), url("../fonts/poppins/poppins-v5-latin-700italic.eot?#iefix") format("embedded-opentype"), url("../fonts/poppins/poppins-v5-latin-700italic.woff2") format("woff2"), url("../fonts/poppins/poppins-v5-latin-700italic.woff") format("woff"), url("../fonts/poppins/poppins-v5-latin-700italic.ttf") format("truetype"), url("../fonts/poppins/poppins-v5-latin-700italic.svg#Poppins") format("svg");
  /* Legacy iOS */ }
/* poppins-800 - latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 800;
  src: url("../fonts/poppins/poppins-v5-latin-800.eot");
  /* IE9 Compat Modes */
  src: local("Poppins ExtraBold"), local("Poppins-ExtraBold"), url("../fonts/poppins/poppins-v5-latin-800.eot?#iefix") format("embedded-opentype"), url("../fonts/poppins/poppins-v5-latin-800.woff2") format("woff2"), url("../fonts/poppins/poppins-v5-latin-800.woff") format("woff"), url("../fonts/poppins/poppins-v5-latin-800.ttf") format("truetype"), url("../fonts/poppins/poppins-v5-latin-800.svg#Poppins") format("svg");
  /* Legacy iOS */ }
/* poppins-800italic - latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 800;
  src: url("../fonts/poppins/poppins-v5-latin-800italic.eot");
  /* IE9 Compat Modes */
  src: local("Poppins ExtraBold Italic"), local("Poppins-ExtraBoldItalic"), url("../fonts/poppins/poppins-v5-latin-800italic.eot?#iefix") format("embedded-opentype"), url("../fonts/poppins/poppins-v5-latin-800italic.woff2") format("woff2"), url("../fonts/poppins/poppins-v5-latin-800italic.woff") format("woff"), url("../fonts/poppins/poppins-v5-latin-800italic.ttf") format("truetype"), url("../fonts/poppins/poppins-v5-latin-800italic.svg#Poppins") format("svg");
  /* Legacy iOS */ }
/* poppins-900 - latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 900;
  src: url("../fonts/poppins/poppins-v5-latin-900.eot");
  /* IE9 Compat Modes */
  src: local("Poppins Black"), local("Poppins-Black"), url("../fonts/poppins/poppins-v5-latin-900.eot?#iefix") format("embedded-opentype"), url("../fonts/poppins/poppins-v5-latin-900.woff2") format("woff2"), url("../fonts/poppins/poppins-v5-latin-900.woff") format("woff"), url("../fonts/poppins/poppins-v5-latin-900.ttf") format("truetype"), url("../fonts/poppins/poppins-v5-latin-900.svg#Poppins") format("svg");
  /* Legacy iOS */ }
a:focus, a:active {
  text-decoration: none;
  outline: none;
  transition: all 300ms ease 0s;
  -moz-transition: all 300ms ease 0s;
  -webkit-transition: all 300ms ease 0s;
  -o-transition: all 300ms ease 0s;
  -ms-transition: all 300ms ease 0s; }

input, select, textarea {
  outline: none;
  appearance: unset !important;
  -moz-appearance: unset !important;
  -webkit-appearance: unset !important;
  -o-appearance: unset !important;
  -ms-appearance: unset !important; }
  
input::-webkit-outer-spin-button, input::-webkit-inner-spin-button {
  appearance: none !important;
  -moz-appearance: none !important;
  -webkit-appearance: none !important;
  -o-appearance: none !important;
  -ms-appearance: none !important;
  margin: 0; }

input:focus, select:focus, textarea:focus {
  outline: none;
  box-shadow: none !important;
  -moz-box-shadow: none !important;
  -webkit-box-shadow: none !important;
  -o-box-shadow: none !important;
  -ms-box-shadow: none !important; }

input[type=checkbox] {
  appearance: checkbox !important;
  -moz-appearance: checkbox !important;
  -webkit-appearance: checkbox !important;
  -o-appearance: checkbox !important;
  -ms-appearance: checkbox !important; }

input[type=radio] {
  appearance: radio !important;
  -moz-appearance: radio !important;
  -webkit-appearance: radio !important;
  -o-appearance: radio !important;
  -ms-appearance: radio !important; }



input:-webkit-autofill {
  box-shadow: 0 0 0 30px transparent inset;
  -moz-box-shadow: 0 0 0 30px transparent inset;
  -webkit-box-shadow: 0 0 0 30px transparent inset;
  -o-box-shadow: 0 0 0 30px transparent inset;
  -ms-box-shadow: 0 0 0 30px transparent inset;
  background-color: transparent !important; }

img {
  max-width: 100%;
  height: auto; }

figure {
  margin: 0; }

p {
  margin: 0px;
  font-weight: 600;
  color: #fff; }

h2 {
  line-height: 1.2;
  margin: 0;
  padding: 0;
  font-weight: 900;
  color: #fff;
  font-family: 'Poppins';
  font-size: 26px;
  text-transform: uppercase;
  margin-bottom: 10px; }

.clear {
  clear: both; }

body {
  margin-top:0px;
  font-size: 14px;
  line-height: 1.8;
  color: #222;
  font-weight: 500;
  font-family: 'Poppins';
  margin: 0px;
  background: #9A616D;
  padding: 35px 0; }

.main {
  position: relative; }

.container {
  width: 1000px;
  margin: 0 auto;
  background: #fff;
  opacity: 0.9;}



.signup-form {
  width: 1015px;
  margin-top: -40px;
  margin-bottom: -40px; }



.register-form {
  padding: 70px 115px 90px 80px;
  margin-bottom: -8px; }

.form-row {
  margin: 0 -30px; }
  .form-row .form-group {
    width: 50%;
    padding: 0 30px; }

.form-input, .form-radio {
  margin-bottom: 15px; }
  
.form-select{
  margin-bottom: 5px; }

label, input {
  display: block;
  width: 100%; }

label {
  font-weight: bold;
  text-transform: uppercase;
  margin-bottom: 7px; }

label.required {
  position: relative; }
  label.required:after {
    content: '*';
    margin-left: 2px;
    color: #b90000; }

input, textarea {
  box-sizing: border-box;
  border: 1px solid #ebebeb;
  padding: 14px 20px;
  border-radius: 5px;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  -o-border-radius: 5px;
  -ms-border-radius: 5px;
  font-size: 14px;
  font-family: 'Poppins'; }
  input:focus {
    border: 1px solid #329e5e; }

.label-flex {
  justify-content: space-between;
  -moz-justify-content: space-between;
  -webkit-justify-content: space-between;
  -o-justify-content: space-between;
  -ms-justify-content: space-between; }
  .label-flex label {
    width: auto; }

.form-link {
  font-size: 12px;
  color: #222;
  text-decoration: none;
  position: relative; }
  .form-link:after {
    position: absolute;
    content: "";
    width: 100%;
    height: 2px;
    background: #d7d7d7;
    left: 0;
    bottom: 12px; }

.form-radio {
  margin-bottom: 5px; }
  .form-radio input {
    width: auto;
    display: inline-block; }

.radio-label {
  padding-right: 72px; }

.form-radio-group {
  padding-bottom: 5px;
  padding-top: 5px; }

.form-radio-item {
  position: relative;
  margin-right: 30px; }
  .form-radio-item label {
    font-weight: 500;
    padding-left: 30px;
    position: relative;
    z-index: 9;
    display: block;
    cursor: pointer;
    text-transform: none; }

.check {
  display: inline-block;
  position: absolute;
  border: 1px solid #ebebeb;
  border-radius: 50%;
  -moz-border-radius: 50%;
  -webkit-border-radius: 50%;
  -o-border-radius: 50%;
  -ms-border-radius: 50%;
  height: 18px;
  width: 18px;
  top: 2px;
  left: 0px;
  z-index: 5;
  transition: border .25s linear;
  -webkit-transition: border .25s linear; }
  .check:before {
    position: absolute;
    display: block;
    content: '';
    width: 12px;
    height: 12px;
    border-radius: 50%;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    -o-border-radius: 50%;
    -ms-border-radius: 50%;
    top: 3px;
    left: 3px;
    margin: auto;
    transition: background 0.25s linear;
    -webkit-transition: background 0.25s linear; }

input[type=radio] {
  position: absolute;
  visibility: hidden; }
  input[type=radio]:checked ~ .check {
    border: 1px solid #329e5e; }
  input[type=radio]:checked ~ .check::before {
    background: #329e5e; }



#slider-margin {
  height: 5px;
  border: none;
  box-shadow: none;
  -moz-box-shadow: none;
  -webkit-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  background: #f8f8f8;
  border-radius: 2.5px;
  -moz-border-radius: 2.5px;
  -webkit-border-radius: 2.5px;
  -o-border-radius: 2.5px;
  -ms-border-radius: 2.5px; }
  #slider-margin .noUi-connect {
    background: #329e5e; }
  #slider-margin .noUi-handle {
    width: 100px;
    height: 30px;
    top: -12px;
    background: #329e5e;
    box-shadow: 0px 3px 2.85px 0.15px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: 0px 3px 2.85px 0.15px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: 0px 3px 2.85px 0.15px rgba(0, 0, 0, 0.1);
    -o-box-shadow: 0px 3px 2.85px 0.15px rgba(0, 0, 0, 0.1);
    -ms-box-shadow: 0px 3px 2.85px 0.15px rgba(0, 0, 0, 0.1);
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -o-border-radius: 5px;
    -ms-border-radius: 5px;
    outline: none;
    border: none;
    right: -50px; }
    #slider-margin .noUi-handle:after, #slider-margin .noUi-handle:before {
      background: transparent;
      height: 0px;
      width: 20px;
      top: -2px;
      font-size: 18px;
      outline: none; }
    #slider-margin .noUi-handle:before {
      font-family: 'Material-Design-Iconic-Font';
      color: #fff;
      content: '\f2fa';
      left: 10px; }
    #slider-margin .noUi-handle:after {
      font-family: 'Material-Design-Iconic-Font';
      color: #fff;
      content: '\f2fb';
      left: auto;
      right: -5px; }
    #slider-margin .noUi-handle .noUi-tooltip {
      border: none;
      background: transparent;
      font-weight: bold;
      color: #fff;
      padding: 0px; }

.price_slider {
  position: relative; }

.form-submit {
  text-align: center; }

.submit {
  width: 150px;
  height: 50px;
  display: inline-block;
  font-family: 'Poppins';
  font-weight: bold;
  font-size: 14px;
  padding: 10px;
  border: none;
  cursor: pointer;
  text-transform: uppercase;
  border-radius: 5px;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  -o-border-radius: 5px;
  -ms-border-radius: 5px; }

#reset {
  background: #fff;
  color: #999;
  border: 2px solid #ebebeb; }
  #reset:hover {
    border: 2px solid #329e5e;
    background: #329e5e;
    color: #fff; }

#submit {
  background: #329e5e;
  color: #fff;
  margin-right: 25px; }
  #submit:hover {
    background-color: #267747; }

.form-input {
  position: relative; }

label.error {
  display: block;
  position: absolute;
  top: 0px;
  right: 0; }
  label.error:after {
    font-family: 'Material-Design-Iconic-Font';
    position: absolute;
    content: '\f1f8';
    right: 20px;
    top: 37px;
    font-size: 23px;
    color: #c70000; }

input.error {
  border: 1px solid #c70000; }

.select-list {
  position: relative;
  display: inline-block;
  width: 100%;
  margin-bottom: 30px; }

.list-item {
  position: absolute;
  width: 100%;
  z-index: 99; }

@media screen and (max-width: 1024px) {
  .container {
    width: calc( 100% - 30px);
    max-width: 100%;
    margin: 0 auto; }

  .signup-img {
    display: none; }

  .signup-form {
    width: 100%; }

  .register-form {
    padding: 93px 80px 90px 80px; } }
@media screen and (max-width: 992px) {
  .signup-content {
    width: 100%; }

  .form-radio-item {
    margin-right: 15px; }

  .register-form {
    padding: 93px 30px 90px 30px; } }
@media screen and (max-width: 768px) {
  .form-row {
    flex-direction: column;
    -moz-flex-direction: column;
    -webkit-flex-direction: column;
    -o-flex-direction: column;
    -ms-flex-direction: column;
    margin: 0px; }

  .form-row .form-group {
    width: 100%;
    padding: 0 0px; } }
@media screen and (max-width: 480px) {
  .submit {
    width: 100%; }

  #submit {
    
    margin-right: 0px; }

  .form-radio-group {
    flex-direction: column;
    -moz-flex-direction: column;
    -webkit-flex-direction: column;
    -o-flex-direction: column;
    -ms-flex-direction: column; } }

</style>
</head>

<body style="background-image:url('https://img.freepik.com/premium-vector/courier-with-parcel-background-delivery-service-van_327176-173.jpg?w=2000'); height: 100%; background-position: center; background-size: cover;">

        <a href="bookcourier.jsp" style="color:white; text-decoration:none; color:Black; padding-bottom:20px; padding-left:450px; font-size:230%; padding-right:20px;"><mark>BOOK COURIER</mark></a>
        <a href="courierdetails.jsp" style="color:white; text-decoration:none; color:Black; padding-bottom:20px; padding-left:50px; font-size:230%; padding-right:20px;"><mark>COURIER DETAILS</mark></a>
        <a href="totalamount.jsp" style="color:white; text-decoration:none; color:Black; padding-bottom:20px; padding-left:50px; font-size:230%; padding-right:20px;"><mark>TOTAL PAYABLE AMOUNT</mark></a>

<div class="main" style="padding-top:20px;">

<div class="container">

<p align="center" style="padding-left:20px; padding-top:20px; font-size:300%; color:white;"><a href="homepage.jsp" style="text-decoration: none; color:white;"><mark style="background-color:#ffcc99; padding:7px;">Book Courier</mark></a></p>
<div class="signup-content">
<div class="signup-form">
<form action="bookcourier.jsp" method="POST" class="register-form" id="register-form">
<div class="form-row">
<div class="form-group">
<div class="form-input">
<label for="first_name" class="required" style="padding-top:5px;">Customer name</label>
<input type="text" name="name" id="first_name" required="required"/>
</div>
<div class="form-input">
<label for="last_name" class="required">PINCODE</label>
<input type="Number" name="pin" id="last_name" required="required"/>
</div>
<div class="form-input">
<label for="last_name" class="required">Packet-Weight In grams</label>
<input type="Number" min=1 max=5000 name="weight" id="last_name" required="required"/>
</div>
<div class="form-input">
<label for="chequeno">Detail Address</label>
<textarea rows="2" cols="46" name="address" required="required"></textarea>
</div>
</div>
<div class="form-group">
<div class="form-select">
<div class="label-flex">
<label for="meal_preference" style="font-size:110%;">Start-End Location</label>
</div>
<div class="select-list">
<select name="Type" id="meal_preference" style="padding-left:20px; height:30px; width:170px; font-size:120%;">
<option value="Banglore-Delhi">Banglore-Delhi</option>
<option value="Banglore-Chennai">Banglore-Chennai</option>
<option value="Banglore-Kolkata">Banglore-Kolkata</option>
<option value="Banglore-Banglore">Banglore-Banglore</option>
<option value="Kolkata-Kolkata">Kolkata-Kolkata</option>
<option value="Chennai-Chennai">Chennai-Chennai</option>
<option value="Chennai-Delhi">Chennai-Delhi</option>
<option value="Chennai-Kolkata">Chennai-Kolkata</option>
<option value="Kolkata-Delhi">Kolkata-Delhi</option>
<option value="Delhi-Delhi">Delhi-Delhi</option>
</select>
</div>
</div>
<div class="form-select">
<div class="label-flex">
<label for="meal_preference" style="font-size:110%;">Gender</label>
</div>
<div class="select-list">
<select name="gender" id="meal_preference" style="padding-left:20px; height:30px; width:100px; font-size:120%;">
<option value="Female">Female</option>
<option value="Male">Male</option>
<option value="Other">Other</option>
</select>
</div>
</div>
<div class="form-input">
<label for="email" class="required">Email</label>
<input type="email" name="email" id="email" required="required"/>
</div>
<div class="form-input">
<label for="phone_number" class="required">Phone number</label>
<input type="tel" name="mobile" id="phone_number" required="required"/>
</div>
</div>
</div>
<div class="form-submit">
<input type="submit" value="Submit" class="submit" id="submit" name="submit" />
</div>

<%
    String fname = request.getParameter("name");
    String pin = request.getParameter("pin");
    String weight = request.getParameter("weight");
    String se = request.getParameter("Type");
    String gender = request.getParameter("gender");
    String address = request.getParameter("address");
    String email = request.getParameter("email");
    String phone = request.getParameter("mobile");
	
    LocalDate shipment = LocalDate.now();
    LocalDate delivery = LocalDate.now().plusDays(2);  
    
    if(fname != null){
    Double amt, we;
    int w=1,g=1;
    if(weight != null){
		we = Double.parseDouble(weight);
		if(we>0 && we<500){
			w = 50;
        }
		else if(we>=500 && we<1500){
			w = 100;
        }
		else if(we>=1500 && we<2500){
			w = 400;
        }
		else if(we>=2500 && we<4000){
			w = 700;
        }
		else if(we>=4000){
			w = 1000;
        }
    }
    
    if(se != null){
    	if(se.equals("Delhi-Delhi") || se.equals("Banglore-Banglore") || se.equals("Kolkata-Kolkata") || se.equals("Chennai-Chennai")){
        	delivery = LocalDate.now().plusDays(2); 
        	g = 2;
        }
        else if(se.equals("Banglore-Chennai") || se.equals("Kolkata-Delhi")){
        	delivery = LocalDate.now().plusDays(4); 
        	g = 4;
        }
        else if(se.equals("Chennai-Delhi") || se.equals("Banglore-Delhi")){
        	delivery = LocalDate.now().plusDays(6); 
        	g = 7;
        }
        else if(se.equals("Chennai-Kolkata") || se.equals("Banglore-Kolkata")){
        	delivery = LocalDate.now().plusDays(8); 
        	g = 9;
        }
    }
    
    amt = (double) w*g;
    
    Methods obj = new Methods();
    
    obj.book_courier(fname, pin, weight, address, se, gender, email, phone, shipment, delivery, amt);
    
	out.println("<br><p align='center' style='color:red; font-size:125%;'>YOU HAVE TO PAY RS. "+amt+"</p>");
    }
    %>
   </form> 
   
</div>

     
</div>
</div>
</div>

</body>
</html>