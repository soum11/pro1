package Assessment3.ThirdAssessment;

import java.sql.*;
public class WorkerData {
	
	public Statement getStatement(){
        Connection con = null;
        Statement stmt = null;

        try{
            Class.forName("com.mysql.jdbc.Driver");

            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/First", "root", "password");
            stmt = con.createStatement();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return stmt;
    }

    public void querys(String query, int i){
        try{

            String sql = query;

            ResultSet rs = getStatement().executeQuery(sql);
            System.out.println("---------------------------------------QUERY "+i+"---------------------------------------");
            if(i == 1){
                System.out.println("FULL_NAME");
            }
            else if(i == 2){
                System.out.println("DISTINCT_DEPARTMENT");
            }
            else if(i == 3){
                System.out.print("The Position of 'a' is : ");
            }

            while(rs.next()){
                System.out.println(rs.getString(1));
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        WorkerData obj = new WorkerData();

        String sql1 = "select concat(" +
                "UPPER (FIRST_NAME)," + " ' ', " +
                "UPPER (LAST_NAME)" +
                ") as FULL_NAME from Worker_Table;";
        String sql2 = "select distinct DEPARTMENT from Worker_Table;";
        String sql3 = "select instr(BINARY FIRST_NAME, 'a') from Worker_Table where FIRST_NAME = 'Amitabh';";

        obj.querys(sql1, 1);
        obj.querys(sql2, 2);
        obj.querys(sql3, 3);
    }
}
