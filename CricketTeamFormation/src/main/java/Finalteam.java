

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Finalteam")
public class Finalteam extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String i = request.getParameter("bowlers");

        CricketTeam obj = new CricketTeam();
		
        List<PlayerInfo> data = new ArrayList<>();
        int bowlers = 6;
        if(i != null) {
        	int fin = Integer.parseInt(i);
        	
        	if(obj.bowlers() >= 3 && obj.wicketkeeper() == 1) {
        		if(obj.count() == 20) {
            		if(fin <= bowlers) {
                    	data = obj.final_team(bowlers, fin);
                		
                		RequestDispatcher rd = request.getRequestDispatcher("Finalteam.html");
                		rd.include(request, response);
                		
                		Iterator<PlayerInfo> itr = data.iterator();
                		out.println("<h1 align='center' style='font-size:150%;'>FINAL TEAM INFORMATION</h1><br><br>");
                		out.println("<table align='center' border-style:'hiden' style='width:65%; font-size:120%; padding-top:-1000px;'><tr align='center'><th>Id</th>"
                				+ "<th>NAME</th>"
                				+ "<th>MATCHES PLAYED</th>"
                				+ "<th>RUNS</th>"
                				+ "<th>WICKETS</th>"
                				+ "<th>ZERO SCORE</th>"
                				+ "<th>TYPE</th><th>AVERAGE</th></tr>");
                		
                		while(itr.hasNext()){
                            PlayerInfo play = itr.next();
                            out.println("<tr align='center'><td>"+play.getId()+"</td><td>"+play.getName()+
                    				"</td><td>"+play.getMatch_played()+
                    				"</td><td>"+play.getRuns()+
                    				"</td><td>"+play.getWickets()+
                    				"</td><td>"+play.getZero_s()+
                    				"</td><td>"+play.getType()+
                    				"</td><td>"+play.getAverage()+
                    				"</td></tr>");
                            
                		}
                		
                		out.println("</table>");
                    }
                	else {
                    	RequestDispatcher rd = request.getRequestDispatcher("Finalteam.html");
                		rd.include(request, response);
                		
                		out.println("<h1 align='center' style='color:red;'>Enter final bowlers whish shoud be less than or equal to "+bowlers+"</h1><br><br>");
                    }
                }
                
                else {
                	RequestDispatcher rd = request.getRequestDispatcher("Finalteam.html");
            		rd.include(request, response);
            		
            		out.println("<h1 align='center' style='color:red;'>Total number of players are not equal to 20, either add or delete Player</h1><br><br>");
                }
            }
        	else {
            	RequestDispatcher rd = request.getRequestDispatcher("Finalteam.html");
        		rd.include(request, response);
        		
        		if(obj.wicketkeeper() != 1) {
            		out.println("<h1 align='center' style='color:red;'>There should be atleast 1 WicketKeeper in Entire 20 players team</h1><br><br>");

        		}
        		if(obj.bowlers() < 3) {
            		out.println("<h1 align='center' style='color:red;'>There should be atleast 3 bowlers in Entire 20 players team</h1><br><br>");

        		}
            }
        }
        	
        else {
        	RequestDispatcher rd = request.getRequestDispatcher("Finalteam.html");
    		rd.include(request, response);
    		
    		out.println("<h1 align='center' style='color:red;'>Enter Data</h1><br><br>");
        }
        	
		
		
		out.close();
	}

}
