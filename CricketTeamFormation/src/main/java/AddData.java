

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/AddData")
public class AddData extends HttpServlet{
	private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String i = request.getParameter("id");
		String fname = request.getParameter("name");
		String m = request.getParameter("mp");
		String rns = request.getParameter("runs");
		String w = request.getParameter("wc");
		String z = request.getParameter("zs");
		String type = request.getParameter("Type");
		
		if(i != null && m != null && rns != null && w != null && z != null) {
			int id = Integer.parseInt(i);
			int mp = Integer.parseInt(m);
			int runs = Integer.parseInt(rns);
			int wc = Integer.parseInt(w);
			int zs = Integer.parseInt(z);
			
			CricketTeam obj = new CricketTeam();
			
			
			
			if((!obj.idcheck(id)) && (!obj.namecheck(fname))) {
				if((mp == zs && runs == 0) || mp > zs) {
					obj.add_player(id, fname, mp, runs, wc, zs, type);
					RequestDispatcher rs = request.getRequestDispatcher("AddData.html");
					rs.include(request, response);
					out.println("<p align='center' style='color:red; font-size:125%;'>Player Added Successfully</p>");
				}
				else{
					obj.add_player(id, fname, mp, runs, wc, zs, type);
					RequestDispatcher rs = request.getRequestDispatcher("AddData.html");
					rs.include(request, response);
					out.println("<p align='center' style='color:red; font-size:125%;'>Enter Proper data, take care of<br> Matches Played, Runs and Zero Score "
							+ "values</p>");
				}
				
			}
			else {
				RequestDispatcher rs = request.getRequestDispatcher("AddData.html");
				rs.include(request, response);
				if(obj.idcheck(id)) {
					out.println("<p align='center' style='color:red; font-size:125%;'>Id already exists, enter different Id to proceed</p>");
				}
				if(obj.namecheck(fname)) {
					out.println("<p align='center' style='color:red; font-size:125%;'>Name already exists, enter different Name to proceed</p>");
				}
			}
			
		}
		else {
			RequestDispatcher rd = request.getRequestDispatcher("AddData.html");
			rd.include(request, response);
			out.println("<p align='center' style='color:red; font-size:125%;'>Enter Data</p>");

		}
		
		

	}

}
