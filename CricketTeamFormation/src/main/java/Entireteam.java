

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Iterator;


@WebServlet("/Entireteam")
public class Entireteam extends HttpServlet{
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
        CricketTeam obj = new CricketTeam();
		
        List<PlayerInfo> data = new ArrayList<>();
		data = obj.allplayer();
		
		RequestDispatcher rd = request.getRequestDispatcher("Entireteam.html");
		rd.include(request, response);
		
		Iterator<PlayerInfo> itr = data.iterator();
		
		out.println("<table align='center' border-style:'hiden' style='width:65%; font-size:120%; padding-top:-1000px;'><tr align='center'><th>Id</th>"
				+ "<th>NAME</th>"
				+ "<th>MATCHES PLAYED</th>"
				+ "<th>RUNS</th>"
				+ "<th>WICKETS</th>"
				+ "<th>ZERO SCORE</th>"
				+ "<th>TYPE</th><th>AVERAGE</th></tr>");
		
		while(itr.hasNext()){
            PlayerInfo play = itr.next();
            out.println("<tr align='center'><td>"+play.getId()+"</td><td>"+play.getName()+
    				"</td><td>"+play.getMatch_played()+
    				"</td><td>"+play.getRuns()+
    				"</td><td>"+play.getWickets()+
    				"</td><td>"+play.getZero_s()+
    				"</td><td>"+play.getType()+
    				"</td><td>"+play.getAverage()+
    				"</td></tr>");
            
		}
		
		out.println("</table>");
		
		out.close();
	}

}
