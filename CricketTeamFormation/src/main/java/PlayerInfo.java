

import java.util.Comparator;

class AvgComparator implements Comparator<PlayerInfo> {
    public int compare(PlayerInfo e1, PlayerInfo e2){
        return (int)(e2.getAverage()-(e1.getAverage()));
    }
}

class NameComparator implements Comparator<PlayerInfo>{
    public int compare(PlayerInfo e1, PlayerInfo e2){
        return e1.getName().compareTo(e2.getName());
    }
}

class IdComparator implements Comparator<PlayerInfo>{
    public int compare(PlayerInfo e1, PlayerInfo e2){
        return (int)(e1.getId()-(e2.getId()));
    }
}


public class PlayerInfo{
    private int id;
    private String name;
    private int match_played;
    private int runs;
    private int wickets;
    private int zero_s;
    private String type;

    private float average;


    PlayerInfo(){}

    public PlayerInfo(int id, String name, int match_played, int runs, int wickets, int zero_s, String type, float average){
        this.id = id;
        this.name = name;
        this.match_played = match_played;
        this.runs = runs;
        this.wickets = wickets;
        this.zero_s = zero_s;
        this.type = type;
        this.average = average;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMatch_played() {
        return match_played;
    }

    public void setMatch_played(int match_played) {
        this.match_played = match_played;
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getWickets() {
        return wickets;
    }

    public void setWickets(int wickets) {
        this.wickets = wickets;
    }

    public int getZero_s() {
        return zero_s;
    }

    public void setZero_s(int zero_s) {
        this.zero_s = zero_s;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getAverage() {
        return average;
    }

    public void setAverage(float average) {
        this.average = average;
    }
}

