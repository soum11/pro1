

import java.rmi.RemoteException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class CricketTeam implements InterfaceMethods{
	
	public boolean idcheck(int id) {
		try {
			String sql = "select * from Cricket_team where Id = "+id+"";

            ResultSet rs = Util.getStatement().executeQuery(sql);
	        
	        while (rs.next()){
	        	return true;
            } 
		}
		catch (Exception e){
            e.printStackTrace();
        }
		
		return false;
	}
	
	public int count(){
		try {
			String sql = "select COUNT(*) from Cricket_team";

            ResultSet rs = Util.getStatement().executeQuery(sql);
	        
	        while (rs.next()){
	        	return rs.getInt(1);
            } 
		}
		catch (Exception e){
            e.printStackTrace();
        }
		
		return 0;
	}
	
	public int bowlers() {
		try {
			String sql = "select COUNT(*) from Cricket_team where Type = 'Bowler'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
	        
	        while (rs.next()){
	        	return rs.getInt(1);
            } 
		}
		catch (Exception e){
            e.printStackTrace();
        }
		
		return 0;
	}
	
	public int wicketkeeper() {
		try {
			String sql = "select COUNT(*) from Cricket_team where Type = 'WicketKeeper'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
	        
	        while (rs.next()){
	        	return rs.getInt(1);
            } 
		}
		catch (Exception e){
            e.printStackTrace();
        }
		
		return 0;
	}
	
	public boolean namecheck(String name) {
		try {
			String sql = "select * from Cricket_team where Player_name ='"+name+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
	        
	        while (rs.next()){
	        	return true;
            } 
		}
		catch (Exception e){
            e.printStackTrace();
        }
		
		return false;
	}
	
	
	public List<PlayerInfo> final_team(int bowlers, int final_bowlers){
		try {
			String sql = "select * from Cricket_team";

	        ResultSet rs = Util.getStatement().executeQuery(sql);
	        
	        List<PlayerInfo> data = new ArrayList<>();
	        
	        while (rs.next()){
	        	data.add(new PlayerInfo(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getInt(6), rs.getString(7), rs.getFloat(8)));
            }
	        List<PlayerInfo> temp = new ArrayList<>();
	        List<PlayerInfo> final_team = new ArrayList<PlayerInfo>();
	        temp.clear();
	        
	        int j = final_bowlers;
	        Collections.sort(data, new AvgComparator());
	        Iterator<PlayerInfo> itr61 = data.iterator();
	        while(itr61.hasNext()){
	            PlayerInfo play2 = itr61.next();
	            if(play2.getType().equals("Bowler")){
	                final_team.add(play2);
	                j--;
	                if(j == 0) {
	                	break;
	                }
	            }
	        }
	        
	        temp = final_team;
	        
	        Collections.sort(data, new AvgComparator());
	        Iterator<PlayerInfo> itr4 = data.iterator();
	        int i = 11-final_bowlers;
	        while(itr4.hasNext()){
	            PlayerInfo play4 = itr4.next();
	            
	            if(play4.getType().equals("Bowler")){
	            	Iterator<PlayerInfo> itr5 = temp.iterator();
	            	boolean found = false;
	            	while(itr5.hasNext()) {
	            		PlayerInfo play = itr5.next();
	            		found = false;
	            		if(play.getName().equals(play4.getName())) {
	            			found = true;
	            			break;
	            		}
	            	}
	            	if(!found) {
	            		i--;
	                	final_team.add(play4);
	            	}
	            	else {
	            		continue;
	            	}
	            }
	            else {
	            	i--;
	            	final_team.add(play4);
	            }
	            if( i == 0){
	                break;
	            }
	        }
	        
	        Collections.sort(final_team, new NameComparator());
	        
			return final_team;
		}
		catch (Exception e){
	         e.printStackTrace();
	    }
		return null;
	}
	
	public List<PlayerInfo> allplayer() {
		List<PlayerInfo> data = new ArrayList<>();
		try {
			String sql = "select * from Cricket_team ORDER BY Id;";

            ResultSet rs = Util.getStatement().executeQuery(sql);
	        
	        while (rs.next()){
	        	data.add(new PlayerInfo(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getInt(6), rs.getString(7), rs.getFloat(8)));
            }
	        return data;
		}
		catch (Exception e){
            e.printStackTrace();
        }
		return data;
	}
	

	@Override
	public int add_player(int id, String name, int matches_played, int runs, int wickets, int zero_score, String type) {
		String sql;
		try{
			float avg = runs/matches_played;
            sql = "insert into Cricket_team values("+id+", '"+name+"', "+matches_played+", "+runs+",  "+wickets+", "+zero_score+", '"+type+"', "+avg+")";
            Util.getStatement().executeUpdate(sql);

        }
        catch (Exception e){
            e.printStackTrace();
        }
		return 0;
	}

	@Override
	public String delete(String name) throws RemoteException {
		try{
            String sql = "delete from Cricket_team where Player_name ='"+name+"'";
            Util.getStatement().executeUpdate(sql);
            return "Deletion Successfull";
        }
        catch (Exception e){
            e.printStackTrace();
        }
		return "Deletion Failure";
	}
	
	public void avgupdate(String name) {
		String sql = null;
		try{
			sql = "select * from Cricket_team where Player_name ='"+name+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
	        int m,r;
	        while (rs.next()){
	        	m = rs.getInt(3);
	        	r = rs.getInt(4);
	        	sql = "update Cricket_team set Average = "+r/m+" where Player_name ='"+name+"'";
	        	Util.getStatement().executeUpdate(sql);
	        }
	        
	        
        }
        catch (Exception e){
            e.printStackTrace();
        }
	}

	@Override
	public String updatePlayerData(String name, String data_type, String data) throws RemoteException {
		String sql=null;
        try{
        	if(data_type.equals("Player_name")) {
        		if(!(namecheck(data))) {
        			sql = "update Cricket_team set "+data_type+" = '"+data+"' where Player_name ='"+name+"'";
                	Util.getStatement().executeUpdate(sql);
                    return "Updation of "+data_type+" is Successful!!!";
        		}
        		else {
                    return "Entered name already exists, please enter the other data for updation";
        		}
            	
        	}
        	else {
        		sql = "update Cricket_team set "+data_type+" = "+data+" where Player_name ='"+name+"'";
            	Util.getStatement().executeUpdate(sql);
        		if(data_type.equals("Matches_played") || data_type.equals("Runs")) {
                	avgupdate(name);
                    return "Updation of "+data_type+" is Successful!!!";
        		}
        		else {
                    return "Updation of "+data_type+" is Successful!!!";
        		}
        	}
        }
        catch (Exception e){
            e.printStackTrace();
        }
        
        return "Updatation of "+data_type+" failed";
	}

}
