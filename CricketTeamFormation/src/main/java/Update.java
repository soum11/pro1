

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Update")
public class Update extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	       response.setContentType("text/html");
			
			PrintWriter out = response.getWriter();
			
			String name = request.getParameter("name");
			String type = request.getParameter("Type");
			String data = request.getParameter("data");
			
			CricketTeam obj = new CricketTeam();
	        
	        boolean res1 = obj.namecheck(name);
	        
	        if(res1) {
	        	String res = obj.updatePlayerData(name, type, data);
		        RequestDispatcher rd = request.getRequestDispatcher("Update.html");
				rd.include(request, response);
	    		out.println("<h1 align='center' style='color:red;'>"+res+"</h1><br><br>");
	        }
	        else {
	        	RequestDispatcher rd = request.getRequestDispatcher("Update.html");
				rd.include(request, response);

	    		out.println("<h1 align='center' style='color:red;'>Id does not exists</h1><br><br>");
	        }     
	}

}
