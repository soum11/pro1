

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfaceMethods extends Remote{
	public int add_player(int id, String name, int matches_played, int runs, int wickets, int zero_score, String type);
    public String delete(String name) throws RemoteException;
    
    public String updatePlayerData(String name, String data_type, String data) throws RemoteException;
}
