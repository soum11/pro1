
import java.util.*;

public class Cricket_Team {
	
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        List<PlayerInfo> data1 = new ArrayList<PlayerInfo>();

        int id, mp, runs, wc, zs;
        String type, name;
        float avg;

//        data1.add(new PlayerInfo(1, "Joseph", 8, 190, 4, 5, "Batsman", 190/8));
//        data1.add(new PlayerInfo(2, "Veere", 12, 253, 2, 4, "Bowler", 253/12));
//        data1.add(new PlayerInfo(3, "Umesh", 16, 173, 0, 3, "Bowler", 173/16));
//        data1.add(new PlayerInfo(4, "Gourav", 4, 125, 5, 2, "Batsman", 125/4));
//        data1.add(new PlayerInfo(5, "Arpit", 7, 100, 3, 2, "Batsman", 100/7));
//        data1.add(new PlayerInfo(6, "Bharat", 19, 126, 1, 3, "Batsman", 126/19));
//        data1.add(new PlayerInfo(7, "Cipla", 22, 784, 6, 4, "Wicketkeeper", 784/22));
//        data1.add(new PlayerInfo(8, "Darshan", 13, 256, 1, 5, "Batsman", 256/13));
//        data1.add(new PlayerInfo(9, "Ishan", 18, 125, 0, 6, "Batsman", 125/18));
//        data1.add(new PlayerInfo(10, "Jhon", 17, 369, 4, 5, "Bowler", 369/17));
//        data1.add(new PlayerInfo(11, "Kritik", 15, 321, 3, 4, "Batsman", 321/15));
//        data1.add(new PlayerInfo(12, "likith", 20, 213, 4, 3, "Batsman", 213/20));
//        data1.add(new PlayerInfo(13, "Mohan", 21, 145, 3, 2, "Batsman", 145/21));
//        data1.add(new PlayerInfo(14, "Narendra", 23, 245, 2, 5, "Bowler", 245/23));
//        data1.add(new PlayerInfo(15, "Praveen", 7, 156, 0, 1, "Batsman", 156/7));
//        data1.add(new PlayerInfo(16, "Ramesh", 15, 178, 1, 2, "Batsman", 178/15));
//        data1.add(new PlayerInfo(17, "Gopal", 14, 523, 2, 3, "Batsman", 523/14));
//        data1.add(new PlayerInfo(18, "Arjun", 16, 412, 6, 4, "Bowler", 412/16));
//        data1.add(new PlayerInfo(19, "Prithvi", 7, 143, 7, 5, "Bowler", 143/7));
//        data1.add(new PlayerInfo(20, "Somesh", 9, 163, 4, 6, "Batsman", 163/9));

        int bowlers=0;

        //System.out.println(data1.get(0));

        int menu;
        boolean running = true;

        while(running){
            System.out.println("  I. Enter 1 to Display all player \n II. Enter 2 to Update player information by name \n"
            		+ "III. Enter 3 to Display Final team \n" +
                    " IV. Enter 4 to add player information \n  V. Enter 5 to exit");
            System.out.print("Enter operation : ");
            menu = sc.nextInt();

            switch (menu){
                case 1://Display all players
                	Collections.sort(data1, new IdComparator());
                	TeamDisplay obj = new TeamDisplay();
                	obj.team_display(data1);
                    break;

                case 2://Update player information by name
                	List<PlayerInfo> temp = new ArrayList<PlayerInfo>();
                	UpdateTeamByName obj3 = new UpdateTeamByName();
                	temp = obj3.updateByName(data1);
                	data1 = temp;
                    break;

                case 3://Dispay Final team
                    List<PlayerInfo> finalteam = new ArrayList<PlayerInfo>();
                    
                    FinalTeamSelection obj1 = new FinalTeamSelection();
                    TeamDisplay obj2 = new TeamDisplay();
                    
                    finalteam = obj1.team_Selection(data1, bowlers);
                    Collections.sort(finalteam, new NameComparator());
                    obj2.team_display(finalteam);

                    System.out.println(" ");
                    break;

                case 4 ://Adding Player information
                    
                	boolean m = false;
                	int x=5;
                	if(!data1.isEmpty()) {
                		System.out.println("You already have 20 members\nEnter 0 to add again and anything to exit: ");
                		x = sc.nextInt();
                		if(x != 0) {
                			break;
                		}
                	}
                	System.out.print("Enter the number of Bowlers , which should be greater than 3 : ");
                    bowlers = sc.nextInt();
                	InputPlayerInformation obj4 = new InputPlayerInformation();
                	data1 = obj4.userInput(bowlers);
                    break;

                case 5:
                    running = false;
                    System.out.println("******************************************THE END**************************************************");
                    break;

                default:
                    System.out.println("Enter properly ");
                    break;
            }
        }

    }
}
