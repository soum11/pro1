import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class FinalTeamSelection {
	
	public List<PlayerInfo> team_Selection(List<PlayerInfo> data, int bowlers) {
		Scanner sc = new Scanner(System.in);
		
		List<PlayerInfo> temp = new ArrayList<>();
        List<PlayerInfo> final_team = new ArrayList<PlayerInfo>();
        temp.clear();
        
        
        int final_bowlers;
        while(true) {
        	System.out.print("Enter the number of bowlers to be included in 11 memebers team out"
        			+ " of "+bowlers+" members : ");
            final_bowlers = sc.nextInt();
            if(final_bowlers <= bowlers) {
            	break;
            }
            else {
            	System.out.println("ERROR\nENTER again which is less than or equal to number of total bowlers.");
            }
        }

        
        int j = final_bowlers;
        Collections.sort(data, new AvgComparator());
        Iterator<PlayerInfo> itr61 = data.iterator();
        while(itr61.hasNext()){
            PlayerInfo play2 = itr61.next();
            if(play2.getType().equals("Bowler")){
                final_team.add(play2);
                j--;
                if(j == 0) {
                	break;
                }
            }
        }
        
        temp = final_team;
        
        TeamDisplay obj = new TeamDisplay();
    	obj.team_display(temp);
        
        Collections.sort(data, new AvgComparator());
        Iterator<PlayerInfo> itr4 = data.iterator();
        int i = 11-final_bowlers;
        while(itr4.hasNext()){
            PlayerInfo play4 = itr4.next();
            
            if(play4.getType().equals("Bowler")){
            	Iterator<PlayerInfo> itr5 = temp.iterator();
            	boolean found = false;
            	while(itr5.hasNext()) {
            		PlayerInfo play = itr5.next();
            		found = false;
            		if(play.getName().equals(play4.getName())) {
            			found = true;
            			break;
            		}
            	}
            	if(!found) {
            		i--;
                	final_team.add(play4);
            	}
            	else {
            		continue;
            	}
            }
            else {
            	i--;
            	final_team.add(play4);
            }
            if( i == 0){
                break;
            }
        }


		return final_team;
	}

}
