import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class TeamDisplay {
	public void team_display(List<PlayerInfo> data) {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%2s%10s%18s%18s%18s%18s%18s","ID","NAME","MATCH_PLAYED","TOTAL_RUNS","WICKETS","OUT_ON_ZERO","PLAYER_TYPE");
		System.out.println();
		System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
		
        Iterator<PlayerInfo> itr = data.iterator();
        while(itr.hasNext()){
            PlayerInfo play = itr.next();
            System.out.printf("%2d%10s%12d%21d%18d%18d%18s",play.getId(),play.getName(),play.getMatch_played(), play.getRuns(),play.getWickets(),
            		play.getZero_s(),play.getType());
            System.out.println();
        }
        
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
	}
}
