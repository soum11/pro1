import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InputPlayerInformation {
	
	public List<PlayerInfo> userInput(int bowlers) {
		
		Scanner sc = new Scanner(System.in);
		
		List<PlayerInfo> data = new ArrayList<PlayerInfo>();
		
		data.clear();
		int id, mp, runs, wc, zs, remain;
        String type, name;
        float avg;
    	//For WicketKeeper
        System.out.println("Enter wicketkeeper information : ");
        System.out.print("Enter Id : ");
        id = sc.nextInt();
        System.out.print("Enter name : ");
        name = sc.next();
        System.out.print("Enter matches played : ");
        mp = sc.nextInt();
        System.out.print("Enter runs : ");
        runs = sc.nextInt();
        System.out.print("Enter wickets : ");
        wc = sc.nextInt();
        System.out.print("Enter zero Scores : ");
        zs = sc.nextInt();
        type = "WicketKeeper";
        avg = runs/mp;
        data.add(new PlayerInfo(id, name, mp, runs, wc, zs, type, avg));

        //For Bowlers
        System.out.println("Enter Bowlers information : ");
        for(int k=0 ; k<bowlers ; k++){
            System.out.print("Enter Id : ");
            id = sc.nextInt();
            System.out.print("Enter name : ");
            name = sc.next();
            System.out.print("Enter matches played : ");
            mp = sc.nextInt();
            System.out.print("Enter runs : ");
            runs = sc.nextInt();
            System.out.print("Enter wickets : ");
            wc = sc.nextInt();
            System.out.print("Enter zero Scores : ");
            zs = sc.nextInt();
            type = "Bowler";
            avg = runs/mp;
            data.add(new PlayerInfo(id, name, mp, runs, wc, zs, type, avg));
        }
        
        //For batsman
        remain = 20-bowlers-1;
        System.out.println("Enter Batsman's information : ");
        for(int k=0 ; k<remain ; k++){
            System.out.print("Enter Id : ");
            id = sc.nextInt();
            System.out.print("Enter name : ");
            name = sc.next();
            System.out.print("Enter matches played : ");
            mp = sc.nextInt();
            System.out.print("Enter runs : ");
            runs = sc.nextInt();
            System.out.print("Enter wickets : ");
            wc = sc.nextInt();
            System.out.print("Enter zero Scores : ");
            zs = sc.nextInt();
            type = "Batsman";
            avg = runs/mp;
            data.add(new PlayerInfo(id, name, mp, runs, wc, zs, type, avg));
        }

        
		return data;
	}
}
