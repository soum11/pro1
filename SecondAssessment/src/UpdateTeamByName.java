import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class UpdateTeamByName {
	
	public List<PlayerInfo> updateByName(List<PlayerInfo> data) {
		Scanner sc = new Scanner(System.in);
		int runs, mp, wc, zs;
		System.out.print("Enter the player name : ");
        String update_name = sc.next();
        boolean found1 = false;
        Iterator<PlayerInfo> itr1 = data.iterator();
        while(itr1.hasNext()){
            PlayerInfo play1 = itr1.next();
            if(update_name.equals(play1.getName())){
                found1 = true;
                boolean x = true;
                String up_data ;
                int y=5;
                while(x){
                    System.out.println("  I. Enter 'mp' to upadte match played \n II. Enter 'runs' to "
                    		+ "upadte Total runs\nIII. Enter 'wc' to upadte wickets\n" +
                            " IV. Enter 'zs' to upadte Zero score");
                    up_data = sc.next();
                    if(up_data.equals("mp")){
                        System.out.print("Enter matches played : ");
                        mp = sc.nextInt();
                        play1.setMatch_played(mp);
                        play1.setAverage(play1.getRuns()/mp);
                    }
                    else if(up_data.equals("runs")){
                        System.out.print("Enter runs : ");
                        runs = sc.nextInt();
                        play1.setRuns(runs);
                        play1.setAverage(runs/play1.getMatch_played());
                    }
                    else if(up_data.equals("wc")){
                        System.out.print("Enter wikects : ");
                        wc = sc.nextInt();
                        play1.setWickets(wc);
                    }
                    else if(up_data.equals("zs")){
                        System.out.print("Enter zero_s : ");
                        zs = sc.nextInt();
                        play1.setZero_s(zs);
                    }
                    else {
                    	System.out.println("Enter from the given option to update.");
                    }
                    System.out.println("Enter 0 to stop updating , and anything to continue: ");
                    y = sc.nextInt();
                    if( y == 0){
                        x = false;
                    }
                }
            }
        }
        try{
            if(!found1){
                PlayerNotFoundException e = new PlayerNotFoundException("Player "+update_name+" not found");
                throw e;
            }
        }
        catch (PlayerNotFoundException e){
            System.out.println("Player "+update_name+" not found in the database");
        }
        
        return data;
		
	}
}
