<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import = "com.bank.Update.*" %>
    <%@ page import="com.bank.AccDetails.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Data Update</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<style type="text/css">
@media(min-width:768px) {
    body {
        margin-top: 50px;
        
    }
    html, body, #wrapper, #page-wrapper {height: 100%; overflow: hidden;}
}

#wrapper {
    padding-left: 0;    
}

#page-wrapper {
    width: 100%;        
    padding: 0;
    background-color: #fff;
}

@media(min-width:768px) {
    #wrapper {
        padding-left: 225px;
    }

    #page-wrapper {
        padding: 22px 10px;
    }
}

/* Side Navigation */

@media(min-width:768px) {
    .side-nav {
        position: fixed;
        top: 60px;
        left: 225px;
        width: 225px;
        margin-left: -225px;
        border: none;
        border-radius: 0;
        border-top: 1px rgba(0,0,0,.5) solid;
        overflow-y: auto;
        background-color: #222;
        /*background-color: #5A6B7D;*/
        bottom: 0;
        overflow-x: hidden;
        padding-bottom: 40px;
        padding-left:9px;
    }

    .side-nav>li>a {
        
        width: 216px;
        border-bottom: 1px rgba(0,0,0,.3) solid;
    }

    .side-nav li a:hover,
    .side-nav li a:focus {
        outline: none;
        background-color: #E8AA42 !important;
    }
    
    .side-nav>li>form>a {
        width: 225px;
        border-bottom: 1px rgba(0,0,0,.3) solid;
    }

    .side-nav li form a:hover,
    .side-nav li form a:focus {
        outline: none;
        background-color: #E8AA42 !important;
    }
}

.navbar-brand {
    padding: 5px 15px;
}
</style>
</head>
<body>


<div id="throbber" style="display:none; min-height:120px;"></div>
<div id="noty-holder"></div>
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header" style="padding-top:30px; padding-left:250px; height:145px;">
        <a class="navbar-brand" href="homepage.jsp" style="font-size:300%; padding-bottom:20px; font-weight: bold; padding-right:100px; color:white;"><mark style="background-color:#DFF6FF;">Jantha Bank</mark><br><br><br></a>
        <%
        AccountDetsService obj = new AccountDetsService();
        HttpSession session1 = request.getSession();
   		
		String account = (String) session1.getAttribute("accountNumber");
		
		if(account != null){
			String res = obj.name(account);
			
			out.println("<p style='color:white; padding-left:15px; padding-top:60px; font-size:200%;'>Welcome Back "+res.toUpperCase()+" !!</p>");
	        
		}
        %>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
               <h3 style="color:white; padding-top:60px;"> >>SERVICES</h3><br>
               <li><a href="dashboard.jsp" style="padding-left:20px; color:black; font-size:150%; background-color:#FEE0C0;">Home</a></li>
                
                <li>
                    <a href="accountdetails.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Profile</a>
                </li>
                
                <li>
                    <a href="withdraw.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Withdraw</a>
                </li>
                
                <li>
                    <a href="deposit.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Deposit</a>
                </li>
                
                <li>
                    <a href="transfer.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Transfer</a>
                </li>
                
                <li>
                    <a href="update.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Update Details</a>
                </li>
                
                <li>
                    <a href="transactions.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Transactions</a>
                </li>

                
                <li>
                    <a href="creditdashboard.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Credit Card Application</a>
                </li>
                
                <li>
                    <a href="loandashboard.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Vehicle Loan Application</a>
                </li>
                
                <li>
                    <a href="deleteaccountrequest.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Delete</a>
                </li>
                
                <li style="padding-top:10px;">
                    <form id="myform1" action="Logout" method="post">
                         <a onclick="document.getElementById('myform1').submit(); return false;" style="text-decoration:none; padding:13px; padding-left:20px; padding-right:132px;  font-size:150%; color:black; background-color:#FEE0C0;">Logout</a>
                    </form>
                </li>
                
            </ul>
        </div>
    </nav>
    
    <div id="page-wrapper" style="height:100%;">
    <div class="container-fluid" style="padding-top:100px; text-align:center">
     <h3 align="center" style="font-size:305%;">Data Updation</h3><br><br><br>
     <form action="" method="post">
      <p align="center" style="font-size:150%;">
          Enter Password  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="password" name="pwd" required="required" style="height:30px; width:175px; font-size:120%;"><br><br>
          
          
          Select Data Type &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select required="required" name="DataType" style="height:30px; width:175px; font-size:80%;">
          <option value="Customer_name">Fullname</option>
          <option value="Username">Username</option>
          <option value="Password">Password</option>
          <option value="Address">Address</option>
          <option value="Mobile_number">Phone Number</option>
          </select>
          <br><br>
          
          
          Enter New Data  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="newdata" required="required" style="height:30px; width:175px; font-size:120%;"><br><br><br>

            <input type = "submit" value = "Confirm" style="height:35px; width:110px; font-size:105%">
          <br><br>
      </p>
      
      </form>
      <%
        HttpSession session2 = request.getSession();
		
		String acc = (String) session2.getAttribute("accountNumber");
		String pass = request.getParameter("pwd");
		String datatype = request.getParameter("DataType");
		String newdata = request.getParameter("newdata");
		
		if(acc != null){
		if(pass != null) {
			UpdateService obj2 = new UpdateService();
			
			boolean t = obj2.passcheck(acc, pass);
			if(t){
				String res1 = obj2.updateBData(acc, datatype, newdata);
				
				out.println("<p align='center' style='color:red; font-size:200%;'>"+res1+"</p>");
				
			}
			else {
				out.println("");
				
				out.println("<br><p align='center' style='color:red; font-size:125%;'>Password</p>");
			}
		}
		}
		else{
			out.println("GO HOME");
		}
      
      %>
  </div>
   </div>
   </div>
</body>
</html>