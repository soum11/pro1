<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import = "com.bank.Data.*" %>
    <%@ page import = "com.bank.AccDetails.*" %>
    <%@ page import = "java.util.Iterator" %>
    <%@ page import = "java.util.ArrayList" %>
    <%@ page import = "java.util.List" %>
    <%@ page import = "java.sql.ResultSet" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Account Details</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<style type="text/css">
@media(min-width:768px) {
    body {
        margin-top: 50px;
        
    }
    html, body, #wrapper, #page-wrapper {height: 100%; overflow: hidden;}
}

#wrapper {
    padding-left: 0;    
}

#page-wrapper {
    width: 100%;        
    padding: 0;
    background-color: #fff;
}

@media(min-width:768px) {
    #wrapper {
        padding-left: 225px;
    }

    #page-wrapper {
        padding: 22px 10px;
    }
}

/* Side Navigation */

@media(min-width:768px) {
    .side-nav {
        position: fixed;
        top: 60px;
        left: 225px;
        width: 225px;
        margin-left: -225px;
        border: none;
        border-radius: 0;
        border-top: 1px rgba(0,0,0,.5) solid;
        overflow-y: auto;
        background-color: #222;
        /*background-color: #5A6B7D;*/
        bottom: 0;
        overflow-x: hidden;
        padding-bottom: 40px;
        padding-left:9px;
    }

    .side-nav>li>a {
        
        width: 216px;
        border-bottom: 1px rgba(0,0,0,.3) solid;
    }

    .side-nav li a:hover,
    .side-nav li a:focus {
        outline: none;
        background-color: #E8AA42 !important;
    }
    
    .side-nav>li>form>a {
        width: 225px;
        border-bottom: 1px rgba(0,0,0,.3) solid;
    }

    .side-nav li form a:hover,
    .side-nav li form a:focus {
        outline: none;
        background-color: #E8AA42 !important;
    }
}

.navbar-brand {
    padding: 5px 15px;
}
</style>
</head>
<body>


<div id="throbber" style="display:none; min-height:120px;"></div>
<div id="noty-holder"></div>
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header" style="padding-top:30px; padding-left:250px; height:145px;">
        <a class="navbar-brand" href="homepage.jsp" style="font-size:300%; padding-bottom:20px; font-weight: bold; padding-right:100px; color:white;"><mark style="background-color:#DFF6FF;">Jantha Bank</mark><br><br><br></a>
        <%
        AccountDetsService obj = new AccountDetsService();
        HttpSession session1 = request.getSession();
   		
		String account = (String) session1.getAttribute("accountNumber");
		if(account != null){
			String res = obj.name(account);
			
			out.println("<p style='color:white; padding-left:15px; padding-top:60px; font-size:200%;'>Welcome Back "+res.toUpperCase()+" !!</p>");
	        
		}
		
        %>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
               <h3 style="color:white; padding-top:60px;"> >>SERVICES</h3><br>
               <li><a href="dashboard.jsp" style="padding-left:20px; color:black; font-size:150%; background-color:#FEE0C0;">Home</a></li>
                
                <li>
                    <a href="accountdetails.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Profile</a>
                </li>
                
                <li>
                    <a href="withdraw.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Withdraw</a>
                </li>
                
                <li>
                    <a href="deposit.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Deposit</a>
                </li>
                
                <li>
                    <a href="transfer.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Transfer</a>
                </li>
                
                <li>
                    <a href="update.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Update Details</a>
                </li>
                
                <li>
                    <a href="transactions.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Transactions</a>
                </li>
                
                <li>
                    <a href="creditdashboard.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Credit Card Application</a>
                </li>
                
                <li>
                    <a href="loandashboard.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Vehicle Loan Application</a>
                </li>
                
                <li>
                    <a href="deleteaccountrequest.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Delete</a>
                </li>
                
                <li style="padding-top:10px;">
                    <form id="myform1" action="Logout" method="post">
                         <a onclick="document.getElementById('myform1').submit(); return false;" style="text-decoration:none; padding:13px; padding-left:20px; padding-right:132px;  font-size:150%; color:black; background-color:#FEE0C0;">Logout</a>
                    </form>
                </li>
                
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" style="height:100%;">
        <div class="container-fluid" style="padding-top:200px;">
           <%
           HttpSession session2 = request.getSession();
   		
   		   String account1 = (String) session2.getAttribute("accountNumber");
   		
   		   if(account1 != null) {
               
   			 
   			try{
   	            String sql = "select * from BankAccounts where Account_no='"+account1+"'";

   	            ResultSet rs = Util.getStatement().executeQuery(sql);
   	            
   	            while (rs.next()){
   			 
   			 out.println("<p align='center'><table align='center' style='width:50%; border: 3px solid; font-size:120%;'>"+
   		   			"<tr align='center' style='border: 3px solid; height: 50px; '><th style='border: 3px solid; text-align: center;'>ACCOUNT NUMBER</th><td style='border: 3px solid;'>"+account1+"</td></tr>"
   		   		   	+"<tr align='center' style='border: 3px solid; height: 50px;'><th style='border: 3px solid; text-align: center;'>FULL NAME</th><td style='border: 3px solid;'>"+rs.getString(2)+"</td></tr>"
   		   		   	+"<tr align='center' style='border: 3px solid; height: 50px;'><th style='border: 3px solid; text-align: center;'>USER NAME</th><td style='border: 3px solid;'>"+rs.getString(3)+"</td></tr>"
   		   		   	+"<tr align='center' style='border: 3px solid; height: 50px;'><th style='border: 3px solid; text-align: center;'>TYPE</th><td style='border: 3px solid;'>"+rs.getString(6)+"</td></tr>"
   		   		   	+"<tr align='center' style='border: 3px solid; height: 50px;'><th style='border: 3px solid; text-align: center;'>GENDER</th><td style='border: 3px solid;'>"+rs.getString(8)+"</td></tr>"
   		   		   	+"<tr align='center' style='border: 3px solid; height: 50px;'><th style='border: 3px solid; text-align: center;'>ADDRESS</th><td style='border: 3px solid;'>"+rs.getString(9)+"</td></tr>"
   		   		   	+"<tr align='center' style='border: 3px solid; height: 50px;'><th style='border: 3px solid; text-align: center;'>EMAIL-ID</th><td style='border: 3px solid;'>"+rs.getString(10)+"</td></tr>"
   		   		   	+"<tr align='center' style='border: 3px solid; height: 50px;'><th style='border: 3px solid; text-align: center;'>MOBILE NUMBER</th><td style='border: 3px solid;'>"+rs.getString(11)+"</td></tr></table></p>");
   		    }
   			}catch (Exception e){
   	            e.printStackTrace();
   	        }
   			
   		   }
   		   else{
   			   out.println("GO HOME");
   		   }
   		   
           
           %>
       </div>
    </div>
</div>
</body>
</html>