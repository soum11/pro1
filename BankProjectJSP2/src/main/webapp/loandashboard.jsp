<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.bank.Admin.*" %>
    <%@ page import="com.bank.AccDetails.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>LOAN DISTRIBUTOR DASHBOARD</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<style type="text/css">
@media(min-width:768px) {
    body {
        margin-top: 50px;
        
    }
    html, body, #wrapper, #page-wrapper {height: 100%; overflow: hidden;}
}

#wrapper {
    padding-left: 0;    
}

#page-wrapper {
    width: 100%;        
    padding: 0;
    background-color: #fff;
}

@media(min-width:768px) {
    #wrapper {
        padding-left: 225px;
    }

    #page-wrapper {
        padding: 22px 10px;
    }
}

/* Side Navigation */

@media(min-width:768px) {
    .side-nav {
        position: fixed;
        top: 60px;
        left: 225px;
        width: 225px;
        margin-left: -225px;
        border: none;
        border-radius: 0;
        border-top: 1px rgba(0,0,0,.5) solid;
        overflow-y: auto;
        background-color: #222;
        /*background-color: #5A6B7D;*/
        bottom: 0;
        overflow-x: hidden;
        padding-bottom: 40px;
        padding-left:9px;
    }

    .side-nav>li>a {
        
        width: 216px;
        border-bottom: 1px rgba(0,0,0,.3) solid;
    }

    .side-nav li a:hover,
    .side-nav li a:focus {
        outline: none;
        background-color: #E8AA42 !important;
    }
    
    .side-nav>li>form>a {
        width: 225px;
        border-bottom: 1px rgba(0,0,0,.3) solid;
    }

    .side-nav li form a:hover,
    .side-nav li form a:focus {
        outline: none;
        background-color: #E8AA42 !important;
    }
}

.navbar-brand {
    padding: 5px 15px;
}
</style>
</head>
<body>


<div id="throbber" style="display:none; min-height:120px;"></div>
<div id="noty-holder"></div>
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header" style="padding-top:30px; padding-left:250px; height:145px;">
        <a class="navbar-brand" href="homepage.jsp" style="font-size:300%; padding-bottom:20px; font-weight: bold; padding-right:100px; color:white;"><mark style="background-color:#DFF6FF;">Jantha Bank</mark><br><br><br></a>
        <%
        AccountDetsService obj = new AccountDetsService();
        HttpSession session1 = request.getSession();
   		
		String account = (String) session1.getAttribute("accountNumber");
		
		if(account != null){
			String res = obj.name(account);
			
			out.println("<p style='color:white; padding-left:15px; padding-top:60px; font-size:200%;'>Welcome Back "+res.toUpperCase()+" !!</p>");
	        
		}
        %>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
               <h3 style="color:white; padding-top:60px;"> >>SERVICES</h3><br>
               <li><a href="dashboard.jsp" style="padding-left:20px; color:black; font-size:150%; background-color:#FEE0C0;">Home</a></li>
                
                <li>
                    <a href="accountdetails.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Profile</a>
                </li>
                
                <li>
                    <a href="withdraw.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Withdraw</a>
                </li>
                
                <li>
                    <a href="deposit.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Deposit</a>
                </li>
                
                <li>
                    <a href="transfer.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Transfer</a>
                </li>
                
                <li>
                    <a href="update.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Update Details</a>
                </li>
                
                <li>
                    <a href="transactions.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Transactions</a>
                </li>
                
                <li>
                    <a href="creditdashboard.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Credit Card Application</a>
                </li>
                
                <li>
                    <a href="loandashboard.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Vehicle Loan Application</a>
                </li>
                
                <li>
                    <a href="deleteaccountrequest.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Delete</a>
                </li>
                
                <li style="padding-top:10px;">
                    <form id="myform1" action="Logout" method="post">
                         <a onclick="document.getElementById('myform1').submit(); return false;" style="text-decoration:none; padding:13px; padding-left:20px; padding-right:132px;  font-size:150%; color:black; background-color:#FEE0C0;">Logout</a>
                    </form>
                </li>
                
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" style="height:100%;">
    <div class="container-fluid" style="padding-top:150px;">
       <table style='width:100%; font-size:120%;'>
          <tr>
             <td style='padding-bottom:200px; width:50vh; background-color:#009999;'>
             <a style="font-size:200%; color:white; text-decoration:none; padding-right:70px;">>>Services</a><br><br>
             <a href="loanapplication.jsp" style="font-size:150%; padding-left:70px; color:black; text-decoration:none;">1. Apply for Vehicle Loan</a><br>
             <a href="loandetails.jsp" style="font-size:150%; padding-left:70px; color:black; text-decoration:none;">2. Your Loan details</a><br>
             <a href="loanstatus.jsp" style="font-size:150%; padding-left:70px; color:black; text-decoration:none;">3. Loan Application Status</a><br>
             </td>
             <td style='padding-left:40px;'>
             <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRM-R9G53s7kqRktn_vPqQyMvg3eNGGQmfe0w&usqp=CAU' width="550" height="550">
             </td>
             <td style='width:50vh; padding-left:50px; padding-right:20px; color:#ffffff; background-color:#ff9999;'>
                <h2>Requirements</h2>
                <ol style="color:black; font-size:120%;">
                <li>You must be at least 22 years old and it generally goes up to 60 years.</li><br>
                <li>Individuals who have had a job for at least 2 years, with a minimum of 1 year with the current employer
                </li><br>
                <li>The minimum of Rs. 3,00,000 per year, including the income of the spouse/co-applicant</li><br>
                <li>Individuals who have a telephone/post-paid mobile.</li><br>
                
                <li>Those who have been in business for a minimum of 2 years.</li><br>
                <li>A good credit score is anywhere from 750 to 900.</li>
                </ol>
             </td>
          </tr>
       </table>     
       <%
    
         
 		
    
    %>
   </div>
   </div>
   </div>

</body>
</html>