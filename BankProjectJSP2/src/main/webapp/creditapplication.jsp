<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.bank.Admin.*" %>
    <%@ page import="com.bank.AccDetails.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CREDIT CARD APPLICATION</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<style type="text/css">
@media(min-width:768px) {
    body {
        margin-top: 50px;
        
    }
    html, body, #wrapper, #page-wrapper {height: 100%; overflow: hidden;}
}

#wrapper {
    padding-left: 0;    
}

#page-wrapper {
    width: 100%;        
    padding: 0;
    background-color: #fff;
}

@media(min-width:768px) {
    #wrapper {
        padding-left: 225px;
    }

    #page-wrapper {
        padding: 22px 10px;
    }
}

/* Side Navigation */

@media(min-width:768px) {
    .side-nav {
        position: fixed;
        top: 60px;
        left: 225px;
        width: 225px;
        margin-left: -225px;
        border: none;
        border-radius: 0;
        border-top: 1px rgba(0,0,0,.5) solid;
        overflow-y: auto;
        background-color: #222;
        /*background-color: #5A6B7D;*/
        bottom: 0;
        overflow-x: hidden;
        padding-bottom: 40px;
        padding-left:9px;
    }

    .side-nav>li>a {
        
        width: 216px;
        border-bottom: 1px rgba(0,0,0,.3) solid;
    }

    .side-nav li a:hover,
    .side-nav li a:focus {
        outline: none;
        background-color: #E8AA42 !important;
    }
    
    .side-nav>li>form>a {
        width: 225px;
        border-bottom: 1px rgba(0,0,0,.3) solid;
    }

    .side-nav li form a:hover,
    .side-nav li form a:focus {
        outline: none;
        background-color: #E8AA42 !important;
    }
}

.navbar-brand {
    padding: 5px 15px;
}
</style>
</head>
<body>


<div id="throbber" style="display:none; min-height:120px;"></div>
<div id="noty-holder"></div>
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header" style="padding-top:30px; padding-left:250px; height:145px;">
        <a class="navbar-brand" href="homepage.jsp" style="font-size:300%; padding-bottom:20px; font-weight: bold; padding-right:100px; color:white;"><mark style="background-color:#DFF6FF;">Jantha Bank</mark><br><br><br></a>
        <%
        AccountDetsService obj = new AccountDetsService();
        HttpSession session1 = request.getSession();
   		
		String account = (String) session1.getAttribute("accountNumber");
		
		if(account != null){
			String res = obj.name(account);
			
			out.println("<p style='color:white; padding-left:15px; padding-top:60px; font-size:200%;'>Welcome Back "+res.toUpperCase()+" !!</p>");
	        
		}
        %>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
               <h3 style="color:white; padding-top:60px;"> >>SERVICES</h3><br>
               <li><a href="dashboard.jsp" style="padding-left:20px; color:black; font-size:150%; background-color:#FEE0C0;">Home</a></li>
                
                <li>
                    <a href="accountdetails.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Profile</a>
                </li>
                
                <li>
                    <a href="withdraw.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Withdraw</a>
                </li>
                
                <li>
                    <a href="deposit.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Deposit</a>
                </li>
                
                <li>
                    <a href="transfer.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Transfer</a>
                </li>
                
                <li>
                    <a href="update.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Update Details</a>
                </li>
                
                <li>
                    <a href="transactions.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Transactions</a>
                </li>
              
                
                <li>
                    <a href="creditdashboard.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Credit Card Application</a>
                </li>
                
                <li>
                    <a href="loandashboard.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Vehicle Loan Application</a>
                </li>
                
                <li>
                    <a href="deleteaccountrequest.jsp" style="font-size:150%; color:black; background-color:#FEE0C0;">Delete</a>
                </li>
                
                <li style="padding-top:10px;">
                    <form id="myform1" action="Logout" method="post">
                         <a onclick="document.getElementById('myform1').submit(); return false;" style="text-decoration:none; padding:13px; padding-left:20px; padding-right:132px;  font-size:150%; color:black; background-color:#FEE0C0;">Logout</a>
                    </form>
                </li>
                
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" style="height:100%;">
    <div class="container-fluid" style="padding-top:70px; text-align:center">
     <h3 align="center" style="font-size:305%;">Credit Card Application</h3><br>
     <form action="creditapplication.jsp" method="post">
      <p align="center" style="font-size:150%;">
      
      <input type="text" name="fname" placeholder="Full Name" required="required" style="height:30px; width:175px; font-size:80%;"><br><br>
      <input type="password" name="pwd" placeholder="Password" required="required" style="height:30px; width:175px; font-size:80%;"><br><br>
       <input type="date" name="birth" placeholder="Date of Born" required="required" style="height:30px; width:175px; font-size:80%;"><br><br>
          <input min=0 type="Number" name="age" placeholder="Age" required="required" style="height:30px; width:175px; font-size:80%;"><br><br>
                    
          
          <select required="required" name="gender" style="height:30px; width:175px; font-size:80%;">
          <option value="Single">Single</option>
          <option value="Married">Married</option>
          <option value="Divorced">Divorced</option>
          <option value="Widowed">Widowed</option>
          </select><br><br>
          
          <select required="required" name="empstatus" style="height:30px; width:175px; font-size:80%;">
          <option value="Salaried">Salaried</option>
          <option value="Self-employed">Self-employed</option>
          <option value="Unemployed">Unemployed</option>
          </select><br><br>
          
          <input min=0 type="Number" name="income" placeholder="Monthly Income" required="required" style="height:30px; width:175px; font-size:80%;"><br><br>
          
          <input type="text" name="aadhar" placeholder="Aadhar Link" required="required" style="height:30px; width:175px; font-size:80%;"><br><br>
          <input type="text" name="pan" placeholder="Pan Link" required="required" style="height:30px; width:175px; font-size:80%;"><br><br>         
          
          <br>
          <input type = "submit" value = "Confirm" style="height:35px; width:110px; font-size:105%">
          <br>
          
      </p>
      
      </form>
       <%
    
         HttpSession session2 = request.getSession();
	
         String acc = (String) session2.getAttribute("accountNumber");
       
         String fname = request.getParameter("fname");
         String pass = request.getParameter("pwd");
         String date = request.getParameter("birth");
         String age = request.getParameter("age");
         String Mart = request.getParameter("gender");
         String Emp = request.getParameter("empstatus");
         String Income = request.getParameter("income");
         String status = "Pending";
         String aadhar = request.getParameter("aadhar");
         String pan = request.getParameter("pan");
         
         AdminServices obj1 = new AdminServices();
 		
 		if(acc != null){
 			 if(fname != null){
 	 			if(obj1.accountexists(acc, pass)){
 	 				String res1 = obj1.creditappli(acc, fname, date, age, Mart, Emp, Income, aadhar, pan, status);
 	 				out.println("<br><p align='center' style='color:red; font-size:200%;'>"+res1+"</p>");
 	 			}
 	 			else{
 	 				out.println("<br><p align='center' style='color:red; font-size:200%;'>Incorrect Password</p>");
 	 			}
 	 			
 	 		 }
 		}
 		else{
 			out.println("GO HOME");
 		}
 		
    
    %>
   </div>
   </div>
   </div>

</body>
</html>