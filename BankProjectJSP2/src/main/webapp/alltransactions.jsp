<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import = "com.bank.AccDetails.*" %>
    <%@ page import = "com.bank.Data.*" %>
    <%@ page import = "java.util.Iterator" %>
    <%@ page import = "java.util.ArrayList" %>
    <%@ page import = "java.util.List" %>
    <%@ page import = "java.sql.ResultSet" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>All Trasactions</title>
<style type="text/css">
body {
        margin: 0px;
    }
.topnav {
  background-color: #333;
  overflow: hidden;
  height:120px;
}

/* Style the links inside the navigation bar */
.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  text-decoration: none;
  font-size: 17px;
}

h1{font-size:300%;}
.error{color:red};

</style>

</head>
<body bgcolor="#ffe6cc">

    <div class="topnav">
      <a href="homepage.jsp" style="font-size:300%; color:white; padding-top:10px; padding-left:800px;">Jantha Bank</a><br><br><br><br>
      <a href="allusers.jsp" style="padding-top:3px; color:white; font-size:150%; padding-left:50px;">All Accounts</a>
      <a href="alltransactions.jsp" style="padding-top:3px; color:white; font-size:150%; padding-left:70px;">All Trasactions</a>
      <a href="admincreditcard.jsp" style="padding-top:3px; color:white; font-size:150%; padding-left:70px;">Credit card Requests</a>
      <a href="acceptedcredit.jsp" style="padding-top:3px; color:white; font-size:150%; padding-left:70px;">Accepted Credit Card Details</a>
      <a href="allloansappli.jsp" style="padding-top:3px; color:white; font-size:150%; padding-left:70px;">Loan Requests</a>
      <a href="acceptedloan.jsp" style="padding-top:3px; color:white; font-size:150%; padding-left:70px;">Accepted Loan Details</a>
      <a href="admindelete.jsp" style="padding-top:3px; color:white; font-size:150%; padding-left:70px;">Delete Account Requests</a>      
    </div>
    <div>
    <%
	
      AccountDetsService obj1 = new AccountDetsService();
	 
      try{
		 String sql = "select * from Trasactions";
		 ResultSet rs = Util.getStatement().executeQuery(sql);
		 
		out.println("<p style='padding-top:20px;'><table align='center' style='width:80%; font-size:120%;'><tr style='border: 3px solid; text-align: center;'><th style='border: 3px solid; text-align: center;'>SL NO.</th>"
				+ "<th style='border: 3px solid; text-align: center;'>ACCOUNT NUMBER</th>"
			    + "<th style='border: 3px solid; text-align: center;'>DATE TIME</th>"
				+ "<th style='border: 3px solid; text-align: center;'>TYPE</th>"
				+ "<th style='border: 3px solid; text-align: center;'>DESCRIPTION</th>"
				+ "<th style='border: 3px solid; text-align: center;'>TRASACTION AMOUNT</th></tr>");
		
		int i=1;
		
		 while(rs.next()){
			out.println("<tr align='center'><td style='border: 1px solid;'>"+i+"</td><td style='border: 1px solid;'>"+rs.getString(1)+
					"</td><td style='border: 1px solid;'>"+rs.getString(2)+
   				"</td><td style='border: 1px solid;'>"+rs.getString(3)+
   				"</td><td style='border: 1px solid;'>"+rs.getString(4)+
   				"</td><td style='border: 1px solid;'>Rs."+rs.getDouble(5)+
   				"</td></tr>");
			i++;
		 }
		 
		 out.println("</table></p>");
	   }
       catch (Exception e){
		   e.printStackTrace();
       }
    
    %>
    
    </div>
    
</body>
</html>