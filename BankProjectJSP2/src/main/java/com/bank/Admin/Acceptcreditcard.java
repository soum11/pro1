package com.bank.Admin;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Acceptcreditcard
 */
@WebServlet("/Acceptcreditcard")
public class Acceptcreditcard extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String applid=request.getParameter("id");

		AdminServices obj = new AdminServices();

		obj.appverfi(applid, "Accepted");
		
		HttpSession session2 = request.getSession();
			
        String acc = (String) session2.getAttribute("accountNumber");
        
        Date d = new Date();

        String m = d.toString();
        String n = m.substring(4,11);
        String x =  m.substring(24);
        m = n+x;
        System.out.println(x);
        
        String name  = obj.name(applid);
        
        obj.creditcard(acc, "Rs.200000", name, m);

		response.sendRedirect("admincreditcard.jsp");
	}

}
