package com.bank.Admin;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Acceptloan")
public class Acceptloan extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String applid=request.getParameter("id");

		AdminServices obj = new AdminServices();

		obj.loanstatus(applid, "Accepted");
		
		HttpSession session2 = request.getSession();
			
        String acc = (String) session2.getAttribute("accountNumber");
        
        Date d = new Date();

        String m = d.toString();
        String n = m.substring(4,11);
        String x =  m.substring(24);
        m = n+x;
        System.out.println(x);
        
        
        obj.apploan(acc, applid, m);

		response.sendRedirect("allloansappli.jsp");
	}

}
