package com.bank.Admin;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Rejectcreditcard")
public class Rejectcreditcard extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String applid=request.getParameter("id"); 

		AdminServices obj = new AdminServices();

		obj.appverfi(applid, "Rejected");

		response.sendRedirect("admincreditcard.jsp");
	}

}
