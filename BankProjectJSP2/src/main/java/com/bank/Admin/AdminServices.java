package com.bank.Admin;

import java.sql.ResultSet;
import com.bank.Delete.*;
import java.sql.SQLException;
import java.util.Date;

import com.bank.Data.Util;

import com.bank.Withdraw.*;

public class AdminServices {
	
	public boolean adminlogin(String admin, String password) {
		String sql;
		try {
			sql = "select * from Admin_Details where Username='"+admin+"'";
            
			ResultSet rs = Util.getStatement().executeQuery(sql);
            
            if(rs.next()) {
            	if(rs.getString(2).equals(password)) {
            		return true;
            	}
            }
            else {
            	return false;
            }
		}
		catch (Exception e) {
            e.printStackTrace();
        }
		return false;
	}
	
	public boolean applidex(String applid) {
		try {
            String sql = "select * from Credit where Applicaton_Id='"+applid+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
            
            if(rs.next()) {
            	return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		return false;
	}
	
	public String creditappli(String acc, String fname, String date, String age, String mart, String emp, String income, String aadhar, String pan, String stat) {
		String sql, applid;
        
        while(true) {
            int appli = (int)(Math.random()*(999999-100000+1)+100000);
            applid = "JNTABC" + String.valueOf(appli);
        	if(!(applidex(applid))) {
        		break;
        	}
        }
		try{
        	sql = "insert into Credit values('"+acc+"', '"+applid+"', '"+fname+"', '"+date+"', '"+age+"', '"+mart+"', '"+emp+"', '"+income+"', '"+aadhar+"', '"+pan+"', '"+stat+"')";
            Util.getStatement().executeUpdate(sql);
            
            return "Uploaded Successfully";
        }
        catch (Exception e){
            e.printStackTrace();
        }
		return null;
	}
	
	public void appverfi(String applid, String data) {
		try {
            String sql = "select * from Credit where Applicaton_Id='"+applid+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
            
            while(rs.next()) {
            	if(rs.getString(11).equals("Pending")) {
            		try {
            			String sql1 = "update Credit set Status = '"+data+"' where Applicaton_Id='"+applid+"'";
                		Util.getStatement().executeUpdate(sql1);
            		}catch (Exception e) {
                        e.printStackTrace();
                    }
            		break;
            	}
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		
	}
	
	public void loanstatus(String applid, String data) {
		try {
            String sql = "select * from Loan_applications where Applicaton_Id='"+applid+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
            
            while(rs.next()) {
            	if(rs.getString("Status").equals("Pending")) {
            		try {
            			String sql1 = "update Loan_applications set Status = '"+data+"' where Applicaton_Id='"+applid+"'";
                		Util.getStatement().executeUpdate(sql1);
            		}catch (Exception e) {
                        e.printStackTrace();
                    }
            		break;
            	}
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		
	}
	
	public boolean cardnumber(String cardnum) {
		try {
            String sql = "select * from CreditCards where CardNumber='"+cardnum+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
            
            if(rs.next()) {
            	return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		return false;
	}
	
	public boolean accountexists(String acc,String pwd) {
        try {
            String sql = "select * from BankAccounts where Account_no ='"+acc+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);

            while (rs.next()) {
                if (pwd.equals(rs.getString("Password"))) {
                	return true; 
                }
                else{
                	return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return false;
    }
	
	public String name(String applid) {
		try {
			String sql = "select * from Credit where Applicaton_Id='"+applid+"'";

	        ResultSet rs = Util.getStatement().executeQuery(sql);
	        
	        while(rs.next()) {
	        	return rs.getString(3);
	        }
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
        return null;
	}
	
	public void creditcard(String acc, String Limitamount, String fname, String date) {
		String sql, applid;
        
        while(true) {
            int appli = (int)(Math.random()*(999999-100000+1)+100000);
            applid = "JNTABCD" + String.valueOf(appli);
        	if(!(cardnumber(applid))) {
        		break;
        	}
        }
		try{
        	sql = "insert into CreditCards values('"+acc+"', '"+applid+"', '"+Limitamount+"', '"+fname+"', '"+date+"')";
            Util.getStatement().executeUpdate(sql);
        }
        catch (Exception e){
            e.printStackTrace();
        }
	}
	
	
	public boolean loanapplid(String applid) {
		try {
            String sql = "select * from Loan_applications where Applicaton_Id='"+applid+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
            
            if(rs.next()) {
            	return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		return false;
	}
	
	public String loanappli(String acc, String fname, String date, String age, String mart, String emp, String income, String mobile, String vtype, String lamount, String eyears, String aadhar, String pan, String ss, String bsa, String addr, String photo, String sign, String stat) {
		String sql, applid;
        
        while(true) {
            int appli = (int)(Math.random()*(999999-100000+1)+100000);
            applid = "JNTABCD" + String.valueOf(appli);
        	if(!(loanapplid(applid))) {
        		break;
        	}
        }
		try{
        	sql = "insert into Loan_applications values('"+acc+"', '"+applid+"', '"+fname+"', '"+date+"', '"+age+"', '"+mart+"', '"+emp+"', '"+income+"', '"+mobile+"', '"+vtype+"', '"+lamount+"', '"+eyears+"', '"+aadhar+"', '"+pan+"', '"+ss+"', '"+bsa+"', '"+addr+"', '"+photo+"', '"+sign+"', '"+stat+"')";
            Util.getStatement().executeUpdate(sql);
            
            return "Uploaded Successfully";
        }
        catch (Exception e){
            e.printStackTrace();
        }
		return null;
	}
	
	public double amount(String acc) {
        try {
            String sql = "select * from BankAccounts where Account_no='"+acc+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);

            while (rs.next()) {
                return rs.getDouble("Amount");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
	
	
	public void deposit(String acc_no, double amt){
        try{
			
            double current_amt = amount(acc_no);
            double fin_am = current_amt + amt;
            String sql1 = "update BankAccounts set Amount = "+fin_am+" where Account_no='"+acc_no+"'";
            Util.getStatement().executeUpdate(sql1);
        } 
        catch (SQLException ex) {
            throw new RuntimeException(ex);
         }
    }
	
	public void apploan(String acc, String applid, String date) {
        
		try {
			String sql = "select * from Loan_applications where Applicaton_Id='"+applid+"'";

	        ResultSet rs = Util.getStatement().executeQuery(sql);
	        
	        String a=null, b=null, c=null, d=null;
	        while(rs.next()) {
	        	a = rs.getString("FullNmae");
	        	b = rs.getString("LoanAmn");
	        	c = rs.getString("VType");
	        	d = rs.getString("emiyear");
	        }
	        try{
	        	double x = Double.parseDouble(b);
	        	int y = Integer.parseInt(d);
	        	
	        	deposit(acc,x);
	        	WithdrawService obj2 = new WithdrawService();
	        	obj2.passbook(acc, "Credit", "LOAN AMOUNT", x);
	        	
	        	
	        	double emiamt = (x + x*0.08*y*12)/(y*12);
	        	
	        	
	        	sql = "insert into ApprovedLoan values('"+acc+"', '"+applid+"', '"+b+"', '"+c+"', '"+d+"', '"+emiamt+"', '"+a+"', '"+date+"')";
	            Util.getStatement().executeUpdate(sql);
	        }
	        catch (Exception e){
	            e.printStackTrace();
	        }
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public boolean deleteid(String applid) {
		try {
            String sql = "select * from DeleteRequests where Applicaton_Id='"+applid+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
            
            if(rs.next()) {
            	return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		return false;
	}
	
	
	public String deletereq(String acc, String stat) {
        String sql, applid;
        
        while(true) {
            int appli = (int)(Math.random()*(999999-100000+1)+100000);
            applid = "JNTABCDE" + String.valueOf(appli);
        	if(!(deleteid(applid))) {
        		break;
        	}
        }
        Date d = new Date();

        String m = d.toString();
        String n = m.substring(4,11);
        String x =  m.substring(24);
        m = n+x;
		try{
        	sql = "insert into DeleteRequests values('"+acc+"', '"+applid+"', '"+m+"', '"+stat+"')";
            Util.getStatement().executeUpdate(sql);
            return "REQUEST SENT";
        }
        catch (Exception e){
            e.printStackTrace();
        }
		return null;
	}
	
	public void loanclose(String acc){

		try{
            String sql = "delete from ApprovedLoan where Account_No ='"+acc+"'";
            Util.getStatement().executeUpdate(sql);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
	
	public void cardclose(String acc){

		try{
            String sql = "delete from CreditCards where Account_No ='"+acc+"'";
            Util.getStatement().executeUpdate(sql);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
	
	
	public void delete(String acc, String data) {
		try {
            String sql = "select * from DeleteRequests where Account_No='"+acc+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
            
            while(rs.next()) {
            	if(rs.getString(4).equals("Pending")) {
            		try {
            			DeleteService obj = new DeleteService();
            			obj.close(acc);
            			loanclose(acc);
            			cardclose(acc);
            			String sql1 = "update DeleteRequests set Status = '"+data+"' where Account_No='"+acc+"'";
                		Util.getStatement().executeUpdate(sql1);
            		}catch (Exception e) {
                        e.printStackTrace();
                    }
            		break;
            	}
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		
	}
}
	
