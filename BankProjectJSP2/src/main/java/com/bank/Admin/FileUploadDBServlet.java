package com.bank.Admin;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.sql.rowset.serial.SerialBlob;

import com.bank.Data.*;

@WebServlet("/FileUploadDBServlet")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50) // 50MB
public class FileUploadDBServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = null;
        try {
            // Connection to Database
            // (See more in JDBC Tutorial).
            conn = Util.getMyConnection();
            conn.setAutoCommit(false);
            
            HttpSession session2 = request.getSession();
        	
        	String acc = (String) session2.getAttribute("accountNumber");
            
            String fname = request.getParameter("fname");
            String date = request.getParameter("birth");
            String age = request.getParameter("age");
            String Mart = request.getParameter("gender");
            String Emp = request.getParameter("empstatus");
            String Income = request.getParameter("income");
            String status = "Pending";
            
            int appli = (int)(Math.random()*(999999-100000+1)+100000);
            String ap_id = "JNTB" + String.valueOf(appli);
            

            int a=0;
            byte[] adhaarFile = null; 
            byte[] panFile = null;
            // Part list (multi files).
            for (Part part : request.getParts()) {
                String fileName = extractFileName(part);
                if (fileName != null && fileName.length() > 0) {
                    // File data
                    InputStream is = part.getInputStream();
                    // Write to file
                    if(a==0)
                    	adhaarFile = is.readAllBytes();
                    else
                    	panFile = is.readAllBytes();
                }
            }
            
            this.writeToDB(conn, adhaarFile, panFile, acc, ap_id, fname, date, age, Mart, Emp, Income, status);
            conn.commit();

            // Upload successfully!.
            response.sendRedirect(request.getContextPath() + "/home/soumya/Desktop/javalab/BankProjectJSP/src/main/java/com/bank/UploadDb");
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("errorMessage", "Error: " + e.getMessage());
            RequestDispatcher dispatcher = request.getRequestDispatcher("creditapplication.jsp");
            dispatcher.forward(request, response);
        } finally {
            this.closeQuietly(conn);
        }
    }

    private String extractFileName(Part part) {
        // form-data; name="file"; filename="C:\file1.zip"
        // form-data; name="file"; filename="C:\Note\file2.zip"
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                // C:\file1.zip
                // C:\Note\file2.zip
                String clientFileName = s.substring(s.indexOf("=") + 2, s.length() - 1);
                clientFileName = clientFileName.replace("\\", "/");
                int i = clientFileName.lastIndexOf('/');
                // file1.zip
                // file2.zip
                return clientFileName.substring(i + 1);
            }
        }
        return null;
    }


    private void writeToDB(Connection conn, byte[] adhaarFile, byte[] panFile, String acc, String app_id, String fname, String date, String age, String mar, String emp, String income, String stat) throws SQLException {

        String sql = "Insert into Creditss(Account_No, Applicaton_Id, FullNmae, Date, Age, MartStat, EmpStat, Income, Aadhaar_DATA, Pan_DATA, Status) " //
                + " values (?,?,?,?,?,?,?,?,?,?,?) ";
        PreparedStatement pstm = conn.prepareStatement(sql);

        pstm.setString(1, acc);
        pstm.setString(2, app_id);
        pstm.setString(3, fname);
        pstm.setString(4, date);
        pstm.setString(5, age);
        pstm.setString(6, mar);
        pstm.setString(7, emp);
        pstm.setString(8, income);
        Blob adhaarBlob = new SerialBlob(adhaarFile );
        pstm.setBlob(9, adhaarBlob);
        Blob panBlob = new SerialBlob(panFile );
        pstm.setBlob(10, panBlob);
        pstm.setString(11, stat);
        pstm.executeUpdate();
    }

    private void closeQuietly(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
        }
    }

}

