package com.bank.Login;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bank.Registration.*;

@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String user = request.getParameter("user");
		String pass = request.getParameter("pwd");
		
		
		LoginService obj = new LoginService();
		
		RegistrationService obj2 = new RegistrationService();
		
		String acc = obj2.accountnumber(user);
		
		if(acc != null) {
			boolean t = obj.accountexists(user, pass);
			if(t){
				HttpSession session = request.getSession();
				session.setAttribute("accountNumber", String.valueOf(acc));
				
				
				RequestDispatcher rd = request.getRequestDispatcher("dashboard.jsp");
				rd.forward(request, response);
			}
			else {
				out.println("");
				RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
				rd.include(request, response);
				out.println("<p align='center' style='color:red; font-size:125%;'>InCorrect Username or Password</p>");
			}
		}
		else {
			out.println("");
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.include(request, response);
			out.println("<p align='center' style='color:red; font-size:125%;'>Enter Correct Username</p>");
		}
	}

}
