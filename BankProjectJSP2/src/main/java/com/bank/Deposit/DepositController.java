package com.bank.Deposit;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bank.Withdraw.*;

@WebServlet("/DepositController")
public class DepositController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession();
		
		String acc = (String) session.getAttribute("accountNumber");
		String pin = request.getParameter("pin");
		String amount = request.getParameter("amount");
		
		Double amt;
		if(!amount.isEmpty()) {
			amt = Double.parseDouble(amount);
			
			DepositService obj = new DepositService();
			WithdrawService obj2 = new WithdrawService();
			
			boolean re = obj2.pincheck(acc, pin);
			
			if(re) {
				String res = obj.deposit(acc , pin, amt);
				
				obj2.passbook(acc, "CREDIT", "DEPOSIT", amt);
				
				RequestDispatcher rd = request.getRequestDispatcher("deposit.jsp");
				rd.include(request, response);
				out.println("<br><p align='center' style='color:red; font-size:200%;'>"+res+"</p>");
			}
			else {
				out.println("");
				RequestDispatcher rd = request.getRequestDispatcher("deposit.jsp");
				rd.include(request, response);
				out.println("<br><p align='center' style='color:red; font-size:125%;'>Enter Correct PIN</p>");
			}
		}
		else {
			out.println("");
			RequestDispatcher rd = request.getRequestDispatcher("deposit.jsp");
			rd.include(request, response);
			out.println("<p align='center' style='color:red; font-size:125%;'>Enter Amount</p>");
		}
	}

}
