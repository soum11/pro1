package com.bank.Withdraw;

import com.bank.Data.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Date;

public class WithdrawService {
	
	public double exists(String acc, String pin) {
        try {
            String sql = "select * from BankAccounts where Account_no='"+acc+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);

            while (rs.next()) {
                if (pin.equals(rs.getString("PIN"))) {
                	return rs.getDouble("Amount");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
	
	public boolean pincheck(String acc, String pin) {
        try {
            String sql = "select * from BankAccounts where Account_no='"+acc+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);

            while (rs.next()) {
                if (pin.equals(rs.getString("PIN"))) {
                	return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
	
	public String withdraw(String acc, String pin, double amt){
        try{
            double current_amt = exists(acc, pin);

            if(amt <= current_amt){
                double fin_am = current_amt - amt;
                String sql1 = "update BankAccounts set Amount = "+fin_am+" where Account_no='"+acc+"'";
                Util.getStatement().executeUpdate(sql1);
                System.out.println("Withdrawal is Successful");
                return "Withdrawal is Successful";
            }
            else{
            	return "Insufficient Balance";
            }
        } 
        catch (SQLException ex) {
        	ex.printStackTrace();
        }
        return "Withdrawal Unsuccessfull";
    }
	
	public boolean passbook(String acc, String oper, String desc, double amt) {
		LocalDateTime date = LocalDateTime.now();

        try {
        	String sql = "insert into Trasactions values('"+acc+"', '"+date+"', '"+oper+"', '"+desc+"', "+amt+")";
        	Util.getStatement().execute(sql);
        	return true;
        }
        catch (SQLException ex) {
        	ex.printStackTrace();
        }
        return false;
        
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
