package com.bank.Withdraw;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/WithdrawController")
public class WithdrawController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession();
		
		String acc = (String) session.getAttribute("accountNumber");
		String pin = request.getParameter("pin");
		String amount = request.getParameter("amount");
		
		Double amt;
		if(!amount.isEmpty()) {
			amt = Double.parseDouble(amount);
			
			WithdrawService obj = new WithdrawService();
			
			boolean re = obj.pincheck(acc, pin);
			
			if(re) {
				String res = obj.withdraw(acc , pin, amt);
				obj.passbook(acc, "DEBIT", "WITHDRAW", amt);
				
				RequestDispatcher rd = request.getRequestDispatcher("withdraw.jsp");
				rd.include(request, response);
				out.println("<br><p align='center' style='color:red; font-size:200%;'>"+res+"</p>");
			}
			else {
				out.println("");
				RequestDispatcher rd = request.getRequestDispatcher("withdraw.jsp");
				rd.include(request, response);
				out.println("<br><p align='center' style='color:red; font-size:125%;'>Enter Correct PIN</p>");
			}
		}
		else {
			out.println("");
			RequestDispatcher rd = request.getRequestDispatcher("withdraw.jsp");
			rd.include(request, response);
			out.println("<p align='center' style='color:red; font-size:125%;'>Enter Amount</p>");
		}
	}

}
