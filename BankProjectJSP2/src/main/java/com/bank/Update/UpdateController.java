package com.bank.Update;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bank.Login.*;

@WebServlet("/UpdateController")
public class UpdateController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession();
		
		String acc = (String) session.getAttribute("accountNumber");
		String pass = request.getParameter("pwd");
		String datatype = request.getParameter("DataType");
		String newdata = request.getParameter("newdata");
		
		if(acc != null) {
			UpdateService obj2 = new UpdateService();
			
			boolean t = obj2.passcheck(acc, pass);
			if(t){
				String res = obj2.updateBData(acc, datatype, newdata);
				
				RequestDispatcher rd = request.getRequestDispatcher("update.jsp");
				rd.include(request, response);
				out.println("<p align='center' style='color:red; font-size:200%;'>"+res+"</p>");
				
			}
			else {
				out.println("");
				RequestDispatcher rd = request.getRequestDispatcher("update.jsp");
				rd.include(request, response);
				out.println("<br><p align='center' style='color:red; font-size:125%;'>Password</p>");
			}
		}
		else {
			out.println("");
			RequestDispatcher rd = request.getRequestDispatcher("update.jsp");
			rd.include(request, response);
			out.println("<br><p align='center' style='color:red; font-size:125%;'>Enter data</p>");
		}
	}

}
