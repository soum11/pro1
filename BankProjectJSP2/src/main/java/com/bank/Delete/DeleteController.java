package com.bank.Delete;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bank.Login.*;


@WebServlet("/DeleteController")
public class DeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession();
		
		String account = (String) session.getAttribute("accountNumber");
		String user = request.getParameter("user");
		String pass = request.getParameter("pwd");
		 
        DeleteService obj = new DeleteService();
		
		if(obj.accountexists(account, user, pass)) {
			obj.close(account);
			obj.closetrans(account);
			RequestDispatcher rd = request.getRequestDispatcher("popup.jsp");
			rd.forward(request, response);
		}
		else {
			out.println("");
			RequestDispatcher rd = request.getRequestDispatcher("delete.jsp");
			rd.include(request, response);
			out.println("<br><p align='center' style='color:red; font-size:125%;'>Incorrect Username</p>");
		}
	}

}
