package com.bank.Registration;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/RegistrationController")
public class RegistrationController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String fname = request.getParameter("fname");
		String user = request.getParameter("user");
		String pass = request.getParameter("pwd");
		String rpass = request.getParameter("rpwd");
		String type = request.getParameter("Type");
		String amount = request.getParameter("amount");
		String gender = request.getParameter("gender");
		String address = request.getParameter("address");
		String email = request.getParameter("email");
		String phone = request.getParameter("mobile");
		String pin = request.getParameter("pin");
		
		double amt;
		if((!amount.isEmpty())) {
			amt = Double.parseDouble(amount);
			
			
			RegistrationService obj = new RegistrationService();
			
			boolean re = obj.userexits(user);
			
			if(!re) {
				if(pass.equals(rpass)) {
					
					if(type.equals("Savings")) {
						if(amt >= 1000) {
							obj.open_account(fname, user, pass, pin, type, amt, gender, address, email, phone);
							
							String acc = obj.accountnumber(user);
							
							HttpSession session = request.getSession();
							session.setAttribute("accountNumber", String.valueOf(acc));
							
							HttpSession session2 = request.getSession();
							session2.setAttribute("info", user);
							
							RequestDispatcher rd = request.getRequestDispatcher("dashboard.jsp");
							rd.forward(request, response);
						}
						else {
							out.println("");
							RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
							rd.include(request, response);
							out.println("<p align='center' style='color:red; font-size:125%;'>For Savings account minimum amount should be Rs.1000</p>");
						}
					}
					else{
						if(amt >= 1500) {
							obj.open_account(fname, user, pass, pin, type, amt, gender, address, email, phone);
							
							String acc = obj.accountnumber(user);
							
							HttpSession session = request.getSession();
							session.setAttribute("accountNumber", String.valueOf(acc));
							
							HttpSession session2 = request.getSession();
							session2.setAttribute("info", user);
							
							RequestDispatcher rd = request.getRequestDispatcher("dashboard.jsp");
							rd.forward(request, response);
						}
					    else {
					    	out.println("");
							RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
							rd.include(request, response);
							out.println("<p align='center' style='color:red; font-size:125%;'>For Current account minimum amount should be Rs.1500</p>");
				       	}	
					}	
				}
				else {
					out.println("");
					RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
					rd.include(request, response);
					out.println("<p align='center' style='color:red; font-size:125%;'>Password doen't match with retype password</p>");
				}
			}
			else {
				out.println("");
				RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
				rd.include(request, response);
				out.println("<p align='center' style='color:red; font-size:125%;'>Username Exists, Enter other Username</p>");
			}
		}
		else {
			out.println("");
			RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
			rd.include(request, response);
			out.println("<p align='center' style='color:red; font-size:125%;'>Enter Amount</p>");
		}
		
	}

}
