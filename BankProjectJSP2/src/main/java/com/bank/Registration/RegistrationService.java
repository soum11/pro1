package com.bank.Registration;

import java.sql.ResultSet;

import com.bank.Data.*;

public class RegistrationService {
	
	public boolean userexits(String username) {
		try {
            String sql = "select * from BankAccounts where Username='"+username+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
            
            if(rs.next()) {
            	return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		return false;
	}
	
	public boolean accnoexits(String accno) {
		try {
            String sql = "select * from BankAccounts where Account_no='"+accno+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
            
            if(rs.next()) {
            	return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		return false;
	}
	
	public int open_account(String fullname, String username, String password, String pin, String type, double amount, String gender, String address, String email, String phone_number) {
        String sql, acc;
        
        while(true) {
            int acc_no = (int)(Math.random()*(999999-100000+1)+100000);
            acc = "JNTB" + String.valueOf(acc_no);
        	if(!(accnoexits(acc))) {
        		break;
        	}
        }
        
        try{
        	sql = "insert into BankAccounts values('"+acc+"', '"+fullname+"', '"+username+"', '"+password+"', '"+pin+"', '"+type+"', "+amount+", '"+gender+"', '"+address+"', '"+email+"', '"+phone_number+"')";
            return Util.getStatement().executeUpdate(sql);

        }
        catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }
	
	
	public String accountnumber(String username) {
		try {
            String sql = "select * from BankAccounts where Username='"+username+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
            
            if(rs.next()) {
            	return rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;
	}
}
