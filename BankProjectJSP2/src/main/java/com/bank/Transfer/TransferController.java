package com.bank.Transfer;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bank.Withdraw.*;
import com.bank.Registration.*;

@WebServlet("/TransferController")
public class TransferController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession();
		
		String acc = (String) session.getAttribute("accountNumber");
		String pin = request.getParameter("pin");
		String amount = request.getParameter("amount");
		String Racc = request.getParameter("account");
		
		Double amt;
		if(!amount.isEmpty()) {
			amt = Double.parseDouble(amount);
			
			TransferService obj = new TransferService();
			WithdrawService obj2 = new WithdrawService();
			RegistrationService obj3 = new RegistrationService();

			boolean rt = obj2.pincheck(acc, pin);
			
			if(rt) {
				boolean t = obj3.accnoexits(Racc);
				if(t){
					String res = obj.transfer(acc, pin, Racc, amt);
					
					obj2.passbook(acc, "DEBIT", "TRANSFERRED TO"+Racc , amt);
					obj2.passbook(Racc, "CREDIT", "TRANSFERRED FROM"+acc , amt);
					
					
					RequestDispatcher rd = request.getRequestDispatcher("transfer.jsp");
					rd.include(request, response);
					out.println("<br><p align='center' style='color:red; font-size:200%;'>"+res+"</p>");
					
				}
				else {
					out.println("");
					RequestDispatcher rd = request.getRequestDispatcher("transfer.jsp");
					rd.include(request, response);
					out.println("<br><p align='center' style='color:red; font-size:125%;'>InCorrect Pin</p>");
				}
			}
			else {
				out.println("");
				RequestDispatcher rd = request.getRequestDispatcher("transfer.jsp");
				rd.include(request, response);
				out.println("<br><p align='center' style='color:red; font-size:125%;'>Enter Correct PIN</p>");
			}
		}
		else {
			out.println("");
			RequestDispatcher rd = request.getRequestDispatcher("transfer.jsp");
			rd.include(request, response);
			out.println("<br><p align='center' style='color:red; font-size:125%;'>Enter Account number and Amount</p>");
		}
	}

}
