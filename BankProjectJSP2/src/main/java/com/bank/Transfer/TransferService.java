package com.bank.Transfer;


import java.sql.ResultSet;
import java.sql.SQLException;

import com.bank.Data.Util;

public class TransferService {
	
	public double amountexisting(String acc_no) {
		try {
            String sql = "select * from BankAccounts where Account_no ='"+acc_no+"'";

            ResultSet rs = Util.getStatement().executeQuery(sql);
            
            if(rs.next()) {
            	return rs.getDouble("Amount");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		return -1;
	}
	
	public String transfer(String acc, String pin, String tans_acc_no, double amt) {
        try{
            double current_amt_w = amountexisting(acc);
            double current_amt_d = amountexisting(tans_acc_no);
            if(amt <= current_amt_w){
                double fin_am = current_amt_w - amt;
                String sql = "update BankAccounts set Amount = "+fin_am+" where Account_no ='"+acc+"'";
                Util.getStatement().executeUpdate(sql);
                double fin_am1 = current_amt_d + amt;
                String sql1 = "update BankAccounts set Amount = "+fin_am1+" where Account_no ='"+tans_acc_no+"'";
                Util.getStatement().executeUpdate(sql1);
                return "Transfer is Successful";
            }
            else{
                return "Insufficient Balance in Sender's account";
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
}
