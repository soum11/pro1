package com.bank.Data;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Logout")
public class Logout extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
		
		PrintWriter out =response.getWriter();
		
		HttpSession session = request.getSession(true);
		
		String user = (String) session.getAttribute("accountNumber");
		
		RequestDispatcher rd = request.getRequestDispatcher("logout.jsp");
		rd.include(request, response);
		
		out.println("<h2 align='center' style='font-size:255%; padding-top:200px;'>"+user+" Successfully logged out. </h2>");
		
		session.invalidate();
	}

}
