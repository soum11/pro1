package com.bank.Data;


import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


@WebListener
public class Listenerr implements HttpSessionListener {

    public Listenerr() {
        // TODO Auto-generated constructor stub
    }
    
    
    public static ServletContext ctx = null;
    
    public static int total = 0 , current = 0;
    
    
    public void sessionCreated(HttpSessionEvent se)  { 
         // TODO Auto-generated method stub
    	total++;
    	current++;
    	
    	ctx = se.getSession().getServletContext();
    	
    	ctx.setAttribute("totalusers", total);
    	ctx.setAttribute("currentusers", current);
    	
    }

    public void sessionDestroyed(HttpSessionEvent se)  { 
         // TODO Auto-generated method stub
    	current--;
    	
    	ctx.setAttribute("currentusers", current);
    	
    	
    }
	
}
