package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.bank.Withdraw.WithdrawService;

class WithdrawServiceTest {

	@Test
	void testPincheck() {
		WithdrawService ob = new WithdrawService();
		assertTrue(ob.pincheck("JNTB883709","33"));
	}

	@Test
	void testWithdraw() {
		WithdrawService ob = new WithdrawService();
		assertEquals("Withdrawal is Successful", ob.withdraw("JNTB883709","33", 10));
	}

	@Test
	void testPassbook() {
		WithdrawService ob = new WithdrawService();
		assertTrue(ob.passbook("JNTB883709", "DEBIT", "WITHDRAW", 10));
	}

}
