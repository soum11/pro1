package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.bank.Transfer.TransferService;
import com.bank.Withdraw.WithdrawService;

class TransferServiceTest {

	@Test
	void testTransfer() {
		TransferService ob = new TransferService();
		assertEquals("Transfer is Successful", ob.transfer("JNTB883709","33", "JNTB890449", 10));
	}
	
	@Test
	void testPassbook() {
		WithdrawService ob = new WithdrawService();
		assertTrue(ob.passbook("JNTB883709", "DEBIT", "TRANSFERRED TO JNTB890449" , 10));
		assertTrue(ob.passbook("JNTB890449", "CREDIT", "TRANSFERRED FROM JNTB883709" , 10));
	}
}
