package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.bank.Deposit.DepositService;
import com.bank.Withdraw.WithdrawService;

class DepositServiceTest {

	@Test
	void testDeposit() {
		DepositService ob = new DepositService();
		assertEquals("Deposition Successful", ob.deposit("JNTB883709","33", 10));
	}
	
	@Test
	void testPassbook() {
		WithdrawService ob = new WithdrawService();
		assertTrue(ob.passbook("JNTB883709", "CREDIT", "DEPOSIT", 10));
	}

}
