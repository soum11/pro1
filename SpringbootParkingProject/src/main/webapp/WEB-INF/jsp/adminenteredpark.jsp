<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.test.entities.Userentry" %>
    <%@ page import="com.test.service.UserService" %>
    <%@ page import="java.util.List" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ALL ENTERED PARKING LOT</title>
<style type="text/css">
	.error {
		color: red;
		font-style: italic;
	}

body{
margin:0px;
padding:0px;
background-color: #FFE6E6;

}
tr:nth-child(even) {background-color: #90A17D;}
tr:nth-child(odd) {background-color: #C0A080;}
th {
  background-color: #FA9494;

}

</style>
</head>
<body>
        <p align="right" style="background-color: black; margin-top:0px; padding-bottom: 10px; padding-top:10px;">
               <a href="/" style="padding-right:30px; font-size:230%; text-decoration: none; color: white;">Home</a>
               <a href="adminservice" style="padding-right:30px; font-size:230%; text-decoration: none; color: white;">Admin</a>
               <a href="userpagedets" style="padding-right:50px; font-size:230%; text-decoration: none; color: white;">User</a>
        </p>
        
         <h1 align="center">WELCOME ADMIN</h1>
         <p align="center">
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="adminpark" style=" text-decoration: none; font-size:200%; color: black;">Parking requests</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="admindepreq" style=" text-decoration: none; font-size:200%; color: black;">Departure requests</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="currentp" style=" text-decoration: none; font-size:200%; color: black;">All Currently Parked</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="enterpark" style=" text-decoration: none; font-size:200%; color: black;">All which entered Parking lot</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="allparkleft" style=" text-decoration: none; font-size:200%; color: black;">All Left</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="parklot" style=" text-decoration: none; font-size:200%; color: black;">Parking Lot</a>
         </p><br>
         <h2 align="center" style="font-size: 190%;"> ALL VEHICLES ENTERED PARKINGLOT </h2>
         
         <%
            
            HttpSession session1 = request.getSession();
            List<Userentry> data = (List<Userentry>) session1.getAttribute("enter");
                       
            
            out.println("<p align='center' style='padding-top:10px;'><table align='center' border-style:'hiden' style='width:80%; font-size:120%;'> <tr align='center'><th style='text-align: center;'>VEHICLE ID</th>"
    				+ "<th style='ext-align: center;'>IN DATE</th>"
    				+ "<th style='text-align: center;'>IN TIME</th>"
    			    + "<th style='text-align: center;'>OUT DATE</th>"
    	    		+ "<th style='text-align: center;'>OUT TIME</th>"
    				+ "<th style='text-align: center;'>OWNERNAME</th>"
    				+ "<th style='text-align: center;'>VEHICLE TYPE</th>"
    				+ "<th style='text-align: center;'>CURRENT STATUS</th>"
    				+ "<th style='text-align: center;'>TOKEN</th>"
    				+ "<th style='text-align: center;'>VEHICLE NUMBER</th>"
    				+ "<th style='text-align: center;'>PAYMENT STATUS</th>"
    				+ "<th style='text-align: center;'>AMOUNT PAID</th></tr>");
            
            
            
            for(Userentry obj2 : data){
            	out.println("<p align='center'>"+
    		   			"<tr align='center' style='border: 3px solid; height: 25px; '><td style=''>"+obj2.getVehicleId()+"</td>"
    		   		   	+"<td style=''>"+obj2.getIndate()+"</td>"
    		   		   	+"<td style=''>"+obj2.getIntime()+"</td>"
    		   		   	+"<td style=''>"+obj2.getOutdate()+"</td>"
        		   	   	+"<td style=''>"+obj2.getOuttime()+"</td>"
    		   		   	+"<td style=''>"+obj2.getParkername()+"</td>"
    		   		   	+"<td style=''>"+obj2.getVehicletype()+"</td>"
    		   		   	+"<td style=''>"+obj2.getCurrentstatus()+"</td>"
    		   		   	+"<td style=''>"+obj2.getTokennumber()+"</td>"
    		   		   	+"<td style=''>"+obj2.getVehiclenumber()+"</td>"
    		   		   	+"<td style=''>"+obj2.getPaymentstatus()+"</td>"
    		   		    +"<td style=''>"+obj2.getAmount()+"</td>"
    		   		   	+"</tr></p>");
            }
            
	         out.println("</table>");
            
            
            
         
         %>
</body>
</html>