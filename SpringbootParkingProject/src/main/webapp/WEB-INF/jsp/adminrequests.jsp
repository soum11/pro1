<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.test.entities.Userentry" %>
    <%@ page import="com.test.service.UserService" %>
    <%@ page import="java.util.List" %>
    <%@ page import="java.io.PrintWriter" %>
    
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ALL PARKING REQUESTS</title>
<style type="text/css">
	.error {
		color: red;
		font-style: italic;
	}

input[type=submit]:hover {
  background-color: #EC994B;
}

input{
   font-family:inherit;
  font-size: inherit;
  background-color: #DFDFDE;
}

select{
   font-family:inherit;
  font-size: inherit;
  background-color: #DFDFDE;
}

body{
margin:0px;
padding:0px;
background-color: #FFE6E6;

}
tr:nth-child(even) {background-color: #90A17D;}
tr:nth-child(odd) {background-color: #C0A080;}
th {
  background-color: #FA9494;

}

</style>
</head>
<body>   <p align="right" style="background-color: black; margin-top:0px; padding-bottom: 10px; padding-top:10px;">
               <a href="/" style="padding-right:30px; font-size:230%; text-decoration: none; color: white;">Home</a>
               <a href="adminservice" style="padding-right:30px; font-size:230%; text-decoration: none; color: white;">Admin</a>
               <a href="userpagedets" style="padding-right:50px; font-size:230%; text-decoration: none; color: white;">User</a>
        </p>
        
         <h1 align="center">WELCOME ADMIN</h1>
         <p align="center">
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="adminpark" style=" text-decoration: none; font-size:200%; color: black;">Parking requests</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="admindepreq" style=" text-decoration: none; font-size:200%; color: black;">Departure requests</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="currentp" style=" text-decoration: none; font-size:200%; color: black;">All Currently Parked</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="enterpark" style=" text-decoration: none; font-size:200%; color: black;">All which entered Parking lot</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="allparkleft" style=" text-decoration: none; font-size:200%; color: black;">All Left</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="parklot" style=" text-decoration: none; font-size:200%; color: black;">Parking Lot</a>
         </p><br>
         <h2 align="center" style="font-size: 190%;">ALL PARKING REQUESTS </h2>
         <%
            
            HttpSession session1 = request.getSession();
            List<Userentry> data = (List<Userentry>) session1.getAttribute("alldata");
           
            
            
            
            out.println("<p align='center' style='padding-top:20px;'><table align='center' border-style:'hiden' style='width:80%; font-size:120%;'> <tr align='center'><th style=' text-align: center;'>VEHICLE ID</th>"
    				+ "<th style='text-align: center;'>IN DATE</th>"
    				+ "<th style='text-align: center;'>IN TIME</th>"
    				+ "<th style='text-align: center;'>OWNERNAME</th>"
    				+ "<th style='text-align: center;'>VEHICLE TYPE</th>"
    				+ "<th style='text-align: center;'>REQUEST STATUS</th>"
    				+ "<th style='text-align: center;'>TOKEN</th>"
    				+ "<th style='text-align: center;'>VEHICLE NUMBER</th>"
    				+ "<th style='text-align: center;'>SELECT FLOOR</th>"
    				+ "<th style='text-align: center;'>SELECT SPOT</th>"
    				+ "<th style=' text-align: center;'>ACCEPT</th>"
    				+ "<th style='text-align: center;'>REJECT</th></tr>");
            
            String data1 = (String) session1.getAttribute("non");
            String data2 = (String) session1.getAttribute("token");
            
            for(Userentry obj2 : data){
            	if(obj2.getRequeststatus().equals("PENDING") || obj2.getRequeststatus().equals("REJECTED")){
            		if(data1 != null && obj2.getVehiclenumber().equals(data2)){
            			if(obj2.getVehicletype().equals("Four-Wheeler")){
                			out.println("<p align='center'>"+
                		   			"<tr align='center' style=''><td style=''>"+obj2.getVehicleId()+"</td>"
                   		   		   	+"<td style=''>"+obj2.getIndate()+"</td>"
                   		   		   	+"<td style=''>"+obj2.getIntime()+"</td>"
                   		   		   	+"<td style=''>"+obj2.getParkername()+"</td>"
                   		   		   	+"<td style=''>"+obj2.getVehicletype()+"</td>"
                      	   		   	+"<td style=''>"+obj2.getRequeststatus()+"</td>"
                        		   	+"<td style=''>"+obj2.getTokennumber()+"</td>"
                        		   	+"<td style=''>"+obj2.getVehiclenumber()+"</td>"
                		   		    +"<td style='cursor:pointer; '><form action='acceptpark' method='post'><select name='floor' style='width:50px;'><option value='A4'>A4</option><option value='B4'>B4</option></select></td>"
                           		    +"<td style='cursor:pointer;'><input type='hidden' required='required' name='vnum' value='"+obj2.getVehiclenumber()+"'/><input min=0 max=10 type='number' required='required' name='tnum' placeholder='Enter Spot'/>&nbsp;"+data1+"</td>"
             		   		     	+"<td style='cursor:pointer; '><input type='submit' value='Accept' /></form></td>"
             		   		        +"<td style='cursor:pointer; '><form action='rejectpark' method='post'><input type='hidden' name='vnum' value='"+obj2.getVehiclenumber()+"'/><input type='submit' value='Reject' /></form></td>"
             		   		     	+"</tr></p>");
                		}
                		if(obj2.getVehicletype().equals("Two-Wheeler")){
                			out.println("<p align='center'>"+
                		   			"<tr align='center' style=''><td style=''>"+obj2.getVehicleId()+"</td>"
                        			+"<td style=''>"+obj2.getIndate()+"</td>"
                           		   	+"<td style=''>"+obj2.getIntime()+"</td>"
                   		   		   	+"<td style=''>"+obj2.getParkername()+"</td>"
                   		   		   	+"<td style=''>"+obj2.getVehicletype()+"</td>"
                   		   		   	+"<td style=''>"+obj2.getRequeststatus()+"</td>"
                   		   		   	+"<td style=''>"+obj2.getTokennumber()+"</td>"
                   		   		   	+"<td style=''>"+obj2.getVehiclenumber()+"</td>"
                        		    +"<td style='cursor:pointer;'><form action='acceptpark' method='post'><select name='floor' style='width:50px;'><option value='A2'>A2</option><option value='B2'>B2</option></select></td>"
                        		    +"<td style='cursor:pointer;'><input type='hidden' required='required' name='vnum' value='"+obj2.getVehiclenumber()+"'/><input min=0 max=10 type='number' required='required' name='tnum' placeholder='Enter Spot'/>"+data1+"</td>"
                		   		   	+"<td style='cursor:pointer;'><input type='submit' value='Accept' /></form></td>"
                		   		    +"<td style='cursor:pointer;'><form action='rejectpark' method='post'><input type='hidden' name='vnum' value='"+obj2.getVehiclenumber()+"'/><input type='submit' value='Reject' /></form></td>"
                		   		   	+"</tr></p>");
                		}
            		}
            		else{
            			if(obj2.getVehicletype().equals("Four-Wheeler")){
                			out.println("<p align='center'>"+
                		   			"<tr align='center' style=''><td style=''>"+obj2.getVehicleId()+"</td>"
                		   		   	+"<td style=''>"+obj2.getIndate()+"</td>"
                		   		   	+"<td style=''>"+obj2.getIntime()+"</td>"
                		   		   	+"<td style=''>"+obj2.getParkername()+"</td>"
                		   		   	+"<td style=''>"+obj2.getVehicletype()+"</td>"
                		   		   	+"<td style=''>"+obj2.getRequeststatus()+"</td>"
                		   		   	+"<td style=''>"+obj2.getTokennumber()+"</td>"
                		   		   	+"<td style=''>"+obj2.getVehiclenumber()+"</td>"
                		   		    +"<td style='cursor:pointer; '><form action='acceptpark' method='post'><select name='floor' style='width:50px;'><option value='A4'>A4</option><option value='B4'>B4</option></select></td>"
                                    +"<td style='cursor:pointer; '><input type='hidden' name='vnum' value='"+obj2.getVehiclenumber()+"'/><input min=0 max=10 type='number' required='required' name='tnum' placeholder='Enter Spot'/></td>"
             		   		     	+"<td style='cursor:pointer;'><input type='submit' value='Accept' /></form></td>"
             		   		        +"<td style='cursor:pointer;'><form action='rejectpark' method='post'><input type='hidden' name='vnum' value='"+obj2.getVehiclenumber()+"'/><input type='submit' value='Reject' /></form></td>"
             		   		     	+"</tr></p>");
                		}
                		if(obj2.getVehicletype().equals("Two-Wheeler")){
                			out.println("<p align='center'>"+
                		   			"<tr align='center' style=''><td style=''>"+obj2.getVehicleId()+"</td>"
                		   		   	+"<td style=''>"+obj2.getIndate()+"</td>"
                		   		   	+"<td style=''>"+obj2.getIntime()+"</td>"
                		   		   	+"<td style=''>"+obj2.getParkername()+"</td>"
                		   		   	+"<td style=''>"+obj2.getVehicletype()+"</td>"
                		   		   	+"<td style=''>"+obj2.getRequeststatus()+"</td>"
                		   		   	+"<td style=''>"+obj2.getTokennumber()+"</td>"
                		   		   	+"<td style=''>"+obj2.getVehiclenumber()+"</td>"
                        		    +"<td style='cursor:pointer;'><form action='acceptpark' method='post'><select name='floor' style='width:50px;'><option value='A2'>A2</option><option value='B2'>B2</option></select></td>"
                        		    +"<td style='cursor:pointer;'><input type='hidden' name='vnum' value='"+obj2.getVehiclenumber()+"'/><input min=0 max=10 type='number' required='required' name='tnum' placeholder='Enter Spot'/></td>"
                		   		   	+"<td style='cursor:pointer;'><input type='submit' value='Accept' /></form></td>"
                		   		    +"<td style='cursor:pointer;'><form action='rejectpark' method='post'><input type='hidden' name='vnum' value='"+obj2.getVehiclenumber()+"'/><input type='submit' value='Reject' /></form></td>"
                		   		   	+"</tr></p>");
                		}
            		}
            	}
            }
            
	         out.println("</table>");
            
            
            
         
         %>
</body>
</html>