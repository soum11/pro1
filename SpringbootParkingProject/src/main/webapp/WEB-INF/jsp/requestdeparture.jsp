<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>REQUEST FOR DEPARTURE</title>
<style type="text/css">

	.error {
		color: red;
		font-style: italic;
	}
	
	input[type=text] {
  padding:10px;
  border-radius:10px;
  border:0;
  box-shadow:0 0 15px 4px rgba(0,0,0,0.06);
   width:15%;
   font-family:inherit;
  font-size: inherit;
}

input[type=number] {
  padding:10px;
  border-radius:10px;
  border:0;
  box-shadow:0 0 15px 4px rgba(0,0,0,0.06);
   width:15%;
   font-family:inherit;
  font-size: inherit;
}

input[type=submit] {
  padding:10px;
  border-radius:10px;
  border:0;
  box-shadow:0 0 15px 4px rgba(0,0,0,0.07);
   width:7%;
   font-family:inherit;
  font-size: inherit;
  background-color: #9CB4CC;
}

input[type=submit]:hover {
  background-color: #EC994B;
}

body{

margin:0px;
padding:0px;
background-color: #FFE6E6;
}


</style>
</head>
<body>
           <p align="right" style="background-color: black; margin-top:0px; padding-bottom: 10px; padding-top:10px;">
               <a href="/" style="padding-right:30px; font-weight: bold; font-size:230%; text-decoration: none; color: white;">Home</a>
               <a href="adminservice" style="padding-right:30px; font-weight: bold; font-size:230%; text-decoration: none; color: white;">Admin</a>
               <a href="userpagedets" style="padding-right:50px; font-weight: bold; font-size:230%; text-decoration: none; color: white;">User</a>
        </p><br>
       
       <a style="font-size: 400%; padding-left:250px; color: black;">&#187;</a>
       <a href="knowamount" style=" text-decoration: none; font-size:200%; color: black;">To know the amount</a>
       <a style="font-size: 400%; padding-left:80px; color: black;">&#187;</a>
       <a href="depart" style="text-decoration: none; font-size:200%; color: black;">Request for departure</a>
       <a style="font-size: 400%; padding-left:80px; color: black;">&#187;</a>
       <a href="departst" style="text-decoration: none; font-size:200%; color: black;">To know the status of departure request</a>
       <br><br><br><br><br><br>
           <h1 align="center" style="font-style: italic; color: #674747;">REQUEST FOR DEPARTURE</h1><br>
           <form align='center' action="departionreq" method="post">
                  <input type="text" name="vehnum" required="required" placeholder="Enter Vehicle number"><br><br>
                  <input type="number" min=0 name="amount" required="required" placeholder="Enter Amount"><br><br><br>
                  <input type="submit" value="PAY">
           </form><br><br>
           
           <%
               HttpSession session1 = request.getSession();
               String data = (String) session1.getAttribute("roq");
               
               if(data != null){
             	  out.println("<h4 align='center' style='font-size:130%;'>"+data+"</h4");
               }
           %>
</body>
</html>