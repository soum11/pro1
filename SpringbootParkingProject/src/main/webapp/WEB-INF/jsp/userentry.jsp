<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ENTER VEHICLE DETAILS</title>
<style type="text/css">
	.error {
		color: red;
		font-style: italic;
	}
	
	input[type=text] {
  padding:10px;
  border-radius:10px;
  border:0;
  box-shadow:0 0 15px 4px rgba(0,0,0,0.06);
   width:15%;
   font-family:inherit;
  font-size: inherit;
}

input[type=submit] {
  padding:10px;
  border-radius:10px;
  border:0;
  box-shadow:0 0 15px 4px rgba(0,0,0,0.07);
   width:15%;
   font-family:inherit;
  font-size: inherit;
  background-color: #9CB4CC;
}

input[type=submit]:hover {
  background-color: #EC994B;
}

body{

margin:0px;
padding:0px;
background-color: #FFE6E6;

}

select {
  padding:10px;
  border-radius:10px;
  border:0;
  box-shadow:0 0 15px 4px rgba(0,0,0,0.06);
   width:16%;
   font-family:inherit;
  font-size: inherit;
  background-color: white;
  color:grey;
}


</style>
</head>
<body>   <p align="right" style="background-color: black; margin-top:0px; padding-bottom: 10px; padding-top:10px;">
               <a href="/" style="padding-right:30px; font-weight: bold; font-size:230%; text-decoration: none; color: white;">Home</a>
               <a href="adminservice" style="padding-right:30px; font-weight: bold; font-size:230%; text-decoration: none; color: white;">Admin</a>
               <a href="userpagedets" style="padding-right:50px; font-weight: bold; font-size:230%; text-decoration: none; color: white;">User</a>
        </p><br>
       
       <a style="font-size: 400%; padding-left:60px; color: black;">&#187;</a>
       <a href="userdata" style=" text-decoration: none; font-size:200%; color: black;">If you want to go for parking</a>
       <a style="font-size: 400%; padding-left:70px; color: black;">&#187;</a>
       <a href="usertoken" style="text-decoration: none; font-size:200%; color: black;">If u want to check parking status and token number</a>
       <a style="font-size: 400%; padding-left:70px; color: black;">&#187;</a>
       <a href="departreq" style="text-decoration: none; font-size:200%; color: black;">Request for the departure</a>
       <br><br><br><br><br><br>
             <div>
             <form:form align="center" action="usersubmitdata" method="post" modelAttribute="user">
                 <h1 style="font-style: italic; color: #674747;">CAR ENTRY - STORE DETAILS </h1><br>
                 <form:input type="text" path="parkername" required="required" placeholder="ENTER NAME"/>
                        <form:errors path="parkername" type="text" cssClass="error"/><br><br>
                  
                 <form:input type="text" path="vehiclenumber" required="required" placeholder="ENTER VECHILENUMBER"/>
                        <form:errors path="vehiclenumber" type="text" cssClass="error"/><br><br>
                 
                 <form:select path="vehicletype">
                 <form:option value="Two-Wheeler">TWO-WHEELER</form:option>
                 <form:option value="Four-Wheeler">FOUR-WHEELER</form:option>
                 </form:select>
                        <form:errors path="vehicletype" cssClass="error"/><br><br>
                 
                 <br>    
                 
                 
                 
                 <input type="submit" value="ENTER" style="font-size: 150%; height:50px; width:110px;"/>
             </form:form><br><br>
             
             </div>
             
             <%
             
                HttpSession session1 = request.getSession();
                String data = (String) session1.getAttribute("xox");
                
                if(data != null){
                	out.println("<h4 align='center' style='font-size:110%;'>"+data+"</h4");
                }
                
             
             %>
</body>
</html>