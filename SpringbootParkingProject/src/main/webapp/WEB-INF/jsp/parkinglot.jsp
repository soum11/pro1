<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.test.entities.Userentry" %>
    <%@ page import="com.test.service.UserService" %>
    <%@ page import="java.util.List" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>PARKING LOT</title>
<style type="text/css">
	.error {
		color: red;
		font-style: italic;
	}
body{
margin:0px;
padding:0px;
background-color: #FFE6E6;

}

</style>
</head>
<body>
         <p align="right" style="background-color: black; margin-top:0px; padding-bottom: 10px; padding-top:10px;">
               <a href="/" style="padding-right:30px; font-size:230%; text-decoration: none; color: white;">Home</a>
               <a href="adminservice" style="padding-right:30px; font-size:230%; text-decoration: none; color: white;">Admin</a>
               <a href="userpagedets" style="padding-right:50px; font-size:230%; text-decoration: none; color: white;">User</a>
        </p>
        
         <h1 align="center">WELCOME ADMIN</h1>
         <p align="center">
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="adminpark" style=" text-decoration: none; font-size:200%; color: black;">Parking requests</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="admindepreq" style=" text-decoration: none; font-size:200%; color: black;">Departure requests</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="currentp" style=" text-decoration: none; font-size:200%; color: black;">All Currently Parked</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="enterpark" style=" text-decoration: none; font-size:200%; color: black;">All which entered Parking lot</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="allparkleft" style=" text-decoration: none; font-size:200%; color: black;">All Left</a>
         <a style="font-size: 400%; padding-left:30px; color: black;">&#187;</a>
         <a href="parklot" style=" text-decoration: none; font-size:200%; color: black;">Parking Lot</a>
         </p><br>
         <h2 align="center" style="font-size: 190%;"> PARKING LOT </h2>
         
         <%
            
            HttpSession session1 = request.getSession();
            List<String> data = (List<String>) session1.getAttribute("tokens");
                       
            
            out.println("<p align='center' style='padding-top:20px;'><table align='center' border-style:'hiden' style='width:80%; font-size:120%;'> <tr align='center'><th style='text-align: center;'>Four-Wheeler FLOOR-A</th>"
    				+ "<th style='text-align: center;'>TWO-WHEELER FLOOR-A</th></tr><tr><td style='text-align: center;'><br><br>");
            
            
            for(int i=1; i<=10; i++){
            	String x = "A4-"+ String.valueOf(i);
            	if(i==6){
            		out.println("<br><br>");
            	}
            	if(data.contains(x)){
            		out.println("<button style='background-color:red; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;'>"+x+"</button>");
            	}
            	else{
            		out.println("<button style='background-color:green; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;'>"+x+"</button>");
            	}
            }
            
            out.println("</td><td style='text-align: center;'><br><br>");
            
            for(int i=1; i<=10; i++){
            	String x = "A2-"+ String.valueOf(i);
            	if(i==6){
            		out.println("<br><br>");
            	}
            	if(data.contains(x)){
            		out.println("<button style='background-color:red; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;'>"+x+"</button>");
            	}
            	else{
            		out.println("<button style='background-color:green; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;'>"+x+"</button>");
            	}
            }
            
            out.println("</td></tr><tr><th style='text-align: center;'><br><br>Four-Wheeler FLOOR-B</th><th style='text-align: center;'><br><br>TWO-WHEELER FLOOR-B</th></tr><tr><td style='text-align: center;'><br><br>");
            
            for(int i=1; i<=10; i++){
            	String x = "B4-"+ String.valueOf(i);
            	if(i==6){
            		out.println("<br><br>");
            	}
            	if(data.contains(x)){
            		out.println("<button style='background-color:red; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;'>"+x+"</button>");
            	}
            	else{
            		out.println("<button style='background-color:green; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;'>"+x+"</button>");
            	}
            }
            
            out.println("</td><td style='text-align: center;'><br><br>");
            
            for(int i=1; i<=10; i++){
            	String x = "B2-"+ String.valueOf(i);
            	if(i==6){
            		out.println("<br><br>");
            	}
            	if(data.contains(x)){
            		out.println("<button style='background-color:red; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;'>"+x+"</button>");
            	}
            	else{
            		out.println("<button style='background-color:green; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px;'>"+x+"</button>");
            	}
            }
            
            
            
	         out.println("</td></tr></table>");
            
            
         
         %>
</body>
</html>