<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ADMIN LOGIN</title>
<style type="text/css">
	.error {
		color: red;
		font-style: italic;
	}
	
	input {
  padding:10px;
  border-radius:10px;
  border:0;
  box-shadow:0 0 15px 4px rgba(0,0,0,0.06);
   width:15%;
   font-family:inherit;
  font-size: inherit;
}

input[type=submit] {
  padding:10px;
  border-radius:10px;
  border:0;
  box-shadow:0 0 15px 4px rgba(0,0,0,0.07);
   width:10%;
   font-family:inherit;
  font-size: inherit;
  background-color: #9CB4CC;
}

input[type=submit]:hover {
  background-color: #EC994B;
}

body{

margin:0px;
padding:0px;
background-color: #FFE6E6;

}


</style>
</head>
<body>   <p align="right" style="background-color: black; margin-top:0px; padding-bottom: 10px; padding-top:10px;">
               <a href="/" style="padding-right:30px; font-size:230%; text-decoration: none; color: white;">Home</a>
               <a href="adminservice" style="padding-right:30px; font-size:230%; text-decoration: none; color: white;">Admin</a>
               <a href="userpagedets" style="padding-right:50px; font-size:230%; text-decoration: none; color: white;">User</a>
        </p><br>
        <a style="font-size: 400%; padding-left:650px; color: black;">&#187;</a>
        <a href="adminadd" style="text-decoration: none; font-size:200%; color: black;">Admin add data</a>
        <a style="font-size: 400%; padding-left:150px; color: black;">&#187;</a>
       <a href="adminlog" style="text-decoration: none; font-size:200%; color: black;">Admin login</a><br><br><br><br>
             <form:form align="center" action="adminloginsub" method="post" modelAttribute="adminlog">
                 <h1 style="font-style: italic; color:white;"><mark style="background-color: blue; color:white;">ADMIN LOGIN</mark></h1><br>
                 <form:input type="text" path="admin_username" placeholder="Enter Admin Username"/>
                        <form:errors path="admin_username" type="text" cssClass="error"/><br><br>
                  
                 <form:input type="password" path="password" placeholder="Enter Password"/>
                        <form:errors path="password" type="password" cssClass="error"/><br><br><br>

                 <input type="submit" value="Login" />
             </form:form><br><br>
             
             <%
             
                HttpSession session1 = request.getSession();
                String data = (String) session1.getAttribute("zoz");
                
                if(data != null){
                	out.println("<h4 align='center' style='font-size:110%;'>"+data+"</h4");
                }
                
             
             %>
</body>
</html>