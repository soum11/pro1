package com.test.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.test.dao.UserDao;
import com.test.entities.Userentry;
import com.test.repository.UserRepository;

@Repository
public class UserDaoImpl implements UserDao{
	
	private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public Userentry findById(int id) {
		Userentry obj = userRepository.findByVehicleId(id);
		return obj;
	}

	@Override
	public void add(Userentry user) {
		try {
			userRepository.save(user);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
	}

	@Override
	public List<Userentry> findal() {
		List<Userentry> result = userRepository.findAll();
		return result;
	}

	@Override
	public void update(Userentry user) {
		userRepository.save(user);
		
	}
	
	
	
}
