package com.test.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.test.dao.AdminDao;
import com.test.entities.Admin;
import com.test.repository.AdminRepository;

@Repository
public class AdminDaoImpl implements AdminDao {
	
	private static final Logger logger = LoggerFactory.getLogger(AdminDaoImpl.class);

	
	@Autowired
	private AdminRepository adminRepository;
	
	@Override
	public void add(Admin admin) {
		try {
			adminRepository.save(admin);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Override
	public List<Admin> getall() {
		List<Admin> result = adminRepository.findAll();
		return result;
	}

	@Override
	public Admin findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}
