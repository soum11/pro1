package com.test.entities;


import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;



@Entity
@Table(name = "admindata")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Admin {
	
	@Id
	@GeneratedValue(generator = "ADMIN_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "ADMIN_SEQ", sequenceName = "ADMIN_SEQ", allocationSize=1)
	@Column(name="ADMIN_ID", unique=true, nullable=false, precision=10, scale=0)
	@Getter @Setter
	private Integer id;
	
	@Size(min=4, max=15)
	@NotEmpty(message = "user required")
	private String admin_username;
	
	@Size(min=4, max=15)
	@NotEmpty(message = "password required")
	private String password;
	

	public String getAdmin_username() {
		return admin_username;
	}

	public void setAdmin_username(String admin_username) {
		this.admin_username = admin_username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
}
