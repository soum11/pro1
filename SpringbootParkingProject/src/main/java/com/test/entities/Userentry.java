package com.test.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import javax.websocket.OnMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "userentry")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Userentry {

	private static final long serialVersionUID = 1L;
	public static final Logger obj = LoggerFactory.getLogger(Userentry.class);


	@Id
	@GeneratedValue(generator = "CAR_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "CAR_SEQ", sequenceName = "CAR_SEQ", allocationSize=1)
	@Column(name="VEHICLE_ID", unique=true, nullable=false, precision=10, scale=0)
	@Getter @Setter
	private Integer vehicleId;

	@Column(name="VEHICLE_OWNER", nullable = true, length = 50)
	@Getter @Setter
	@NotEmpty(message = "user required")
	private String parkername;

	@Column(name="VEHICLE_NUMBER", nullable = true, length = 50)
	@Getter @Setter
	@NotEmpty(message = "user required")
	private String vehiclenumber;

	@Column(name="VEHICLE_TYPE", nullable = true, length = 50)
	@Getter @Setter
	@NotEmpty(message = "user required")
	private String vehicletype;

	@Column(name="INDATE", nullable = true, length = 25)
	@Getter @Setter
	private String indate;

	@Column(name="INTIME", nullable = true, length = 25)
	@Getter @Setter
	private String intime;

	@Column(name="OUTDATE", nullable = true, length = 25)
	@Getter @Setter
	private String outdate;

	@Column(name="OUTTIME", nullable = true, length = 25)
	@Getter @Setter
	private String outtime;

	@Column(name="PAYMENT_STATUS", nullable = true, length = 25)
	@Getter @Setter
	private String paymentstatus;

	@Column(name="AMOUNT", nullable = true, length = 25)
	@Getter @Setter
	private String amount;

	@Column(name="TOKEN", nullable = true, length = 25)
	@Getter @Setter
	private String tokennumber;

	@Column(name="REQUEST_STATUS", nullable = true, length = 25)
	@Getter @Setter
	private String requeststatus;

	@Column(name="CURRENT_STATUS", nullable = true, length = 25)
	@Getter @Setter
	private String currentstatus;

	@Column(name="DEPATURE_RS", nullable = true, length = 25)
	@Getter @Setter
	private String parkingoperator;

	public Integer getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getParkername() {
		return parkername;
	}

	public void setParkername(String parkername) {
		this.parkername = parkername;
	}

	public String getVehiclenumber() {
		return vehiclenumber;
	}

	public void setVehiclenumber(String vehiclenumber) {
		this.vehiclenumber = vehiclenumber;
	}

	public String getVehicletype() {
		return vehicletype;
	}

	public void setVehicletype(String vehicletype) {
		this.vehicletype = vehicletype;
	}

	public String getIndate() {
		return indate;
	}

	public void setIndate(String indate) {
		this.indate = indate;
	}

	public String getIntime() {
		return intime;
	}

	public void setIntime(String intime) {
		this.intime = intime;
	}

	public String getOutdate() {
		return outdate;
	}

	public void setOutdate(String outdate) {
		this.outdate = outdate;
	}

	public String getOuttime() {
		return outtime;
	}

	public void setOuttime(String outtime) {
		this.outtime = outtime;
	}

	public String getPaymentstatus() {
		return paymentstatus;
	}

	public void setPaymentstatus(String paymentstatus) {
		this.paymentstatus = paymentstatus;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTokennumber() {
		return tokennumber;
	}

	public void setTokennumber(String tokennumber) {
		this.tokennumber = tokennumber;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public static Logger getObj() {
		return obj;
	}

	public String getRequeststatus() {
		return requeststatus;
	}

	public void setRequeststatus(String requeststatus) {
		this.requeststatus = requeststatus;
	}

	public String getCurrentstatus() {
		return currentstatus;
	}

	public void setCurrentstatus(String currentstatus) {
		this.currentstatus = currentstatus;
	}

	public String getParkingoperator() {
		return parkingoperator;
	}

	public void setParkingoperator(String parkingoperator) {
		this.parkingoperator = parkingoperator;
	}

	



}
