package com.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.test.entities.Userentry;


@Transactional
public interface UserRepository extends JpaRepository<Userentry, Integer>{

	Userentry findByVehicleId(int id);

}
