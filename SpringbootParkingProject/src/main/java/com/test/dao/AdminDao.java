package com.test.dao;

import java.util.List;


import com.test.entities.Admin;

public interface AdminDao{
	
	public Admin findById(int id);
	
	public void add(Admin admin);
	
	public List<Admin> getall();
	
}
