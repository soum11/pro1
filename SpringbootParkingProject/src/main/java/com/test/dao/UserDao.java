package com.test.dao;


import java.util.List;

import com.test.entities.Userentry;

public interface UserDao {
	
	public Userentry findById(int id);
	
	public void add(Userentry user);
	
	public List<Userentry> findal();
	
	public void update(Userentry user);

}
