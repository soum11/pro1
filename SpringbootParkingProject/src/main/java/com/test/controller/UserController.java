package com.test.controller;

import java.lang.runtime.ObjectMethods;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import com.test.entities.Userentry;
import com.test.exception.ResourceNotFoundException;
import com.test.repository.UserRepository;
import com.test.service.UserService;


@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
    private UserRepository userrepo;
	
	
	@GetMapping("/Userdata/{id}")
	public ResponseEntity<Userentry> getVehicleById(@PathVariable(value = "id") int vehicleid)
			throws ResourceNotFoundException {
		Userentry vehicle = userrepo.findById(vehicleid)
				.orElseThrow(() -> new ResourceNotFoundException("Vehicle not found for this id :: " + vehicleid));
		return ResponseEntity.ok().body(vehicle);
	}

	
	
	
	@RequestMapping(value = "/usersubmitdata", method = RequestMethod.POST)
	public String userdata(@Valid @ModelAttribute("user") Userentry usr, HttpServletRequest request, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			return "userentry";
		}
		String nn = userService.vehicletestpp(usr.getVehiclenumber());
		if(nn != null) {
			HttpSession session = request.getSession();
			session.setAttribute("xox", nn);
			return "userentry";
		}
		usr.setOutdate(null);
		usr.setOuttime(null);
		usr.setPaymentstatus("NOT");
		usr.setAmount(null);
		usr.setTokennumber(null);
		usr.setRequeststatus("PENDING");
		usr.setCurrentstatus(null);
		
		LocalDate datenow = LocalDate.now();
		String daten = datenow.toString();
		usr.setIndate(daten);
		
		LocalTime timenow = LocalTime.now();
		String timen = timenow.toString();
		usr.setIntime(timen);
		
		userService.add(usr);
		
		System.out.println(usr.getVehiclenumber()+usr.getParkername()+usr.getVehicletype());
		
		HttpSession session = request.getSession();
		session.setAttribute("xox", "SUCCESSFULLY PARKING REQUEST SENT");
		
		return "userentry";
	}
	
	@RequestMapping("/userdata")
	public String loginPage(Model model, HttpServletRequest request, Userentry usr) {
		HttpSession session = request.getSession();
		session.setAttribute("xox", null);
		model.addAttribute("user", usr);
		return "userentry";
	}
	
	@RequestMapping("/usertoken")
	public String tokenf(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("tot", null);
		return "usertokenfind";
	}
	
	@RequestMapping("/tokenfsub")
	public String tokensub(HttpServletRequest request) {
		
		String venum = request.getParameter("vehnum");
		
		String res = userService.passtoken(venum);
		
		HttpSession session = request.getSession();
		session.setAttribute("tot", res);
		
		return "usertokenfind";
	}
	
	@RequestMapping("/knowamount")
	public String amountf(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("aoa", null);
		return "userknowamout";
	}
	
	@RequestMapping("/getamount")
	public String amountasi(HttpServletRequest request) {
		
		String venum = request.getParameter("vehnum");
		
		boolean res = userService.vehicletest(venum);
		
		if(res) {
			boolean res1 = userService.tokenexists(venum);
			
			if(res1) {
				boolean res2 = userService.parkstatus(venum);
				
				if(res2) {
					HttpSession session = request.getSession();
					session.setAttribute("aoa", "You have already left");
					
					return "userknowamout";
				}
				
				String am = userService.setamount(venum);
				HttpSession session = request.getSession();
				session.setAttribute("aoa", am);
				
				return "userknowamout";
			}
			else {
				HttpSession session = request.getSession();
				session.setAttribute("aoa", "Parking was not available or you are still waiting for token to be assigned");
				
				return "userknowamout";
			}	
		}
		else {
			HttpSession session = request.getSession();
			session.setAttribute("aoa", "Enter valid vehicle number");
			
			return "userknowamout";
		}
		
	}
	
	@RequestMapping("/departreq")
	public String depart(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("tot", null);
		return "depature";
	}
	
	
	@RequestMapping("/depart")
	public String reqdep(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("roq", null);
		return "requestdeparture";
	}
	
	@RequestMapping("/departionreq")
	public String deprequesta(HttpServletRequest request) {
		
		String venum = request.getParameter("vehnum");
		String amount = request.getParameter("amount");
		
		boolean res = userService.vehicletest(venum);
		
		if(res) {
			boolean res1 = userService.tokenexists(venum);
			
			if(res1) {
				boolean res2 = userService.parkstatus(venum);
				
				if(res2) {
					HttpSession session = request.getSession();
					session.setAttribute("roq", "You have already left");
					
					return "requestdeparture";
				}
				
				userService.reqaccept(venum, amount);
				HttpSession session = request.getSession();
				session.setAttribute("roq", "Request sent Successfully");
				
				return "requestdeparture";
			}
			else {
				HttpSession session = request.getSession();
				session.setAttribute("roq", "Parking was rejected");
				
				return "requestdeparture";
			}	
		}
		else {
			HttpSession session = request.getSession();
			session.setAttribute("roq", "Enter valid vehicle number");
			
			return "requestdeparture";
		}
		
	}
	
	
	@RequestMapping("/departst")
	public String depstat(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("dstatus", null);
		return "depaturestatus";
	}
	
	
	@RequestMapping("/departstat")
	public String depstatus(HttpServletRequest request) {
		
		String venum = request.getParameter("vehnum");
		
		boolean res = userService.vehicleteststatus(venum);
		System.out.println(res);
		
		if(res) {
			boolean res1 = userService.tokenexists(venum);
			
			if(res1) {
				boolean res2 = userService.parkstatus(venum);
				
				if(res2) {
					HttpSession session = request.getSession();
					session.setAttribute("dstatus", "You have already left or You Can leave now and Do visit again");
					
					return "depaturestatus";
				}
				
				String dep = userService.departstat(venum);
				HttpSession session = request.getSession();
				session.setAttribute("dstatus", dep);
				
				return "depaturestatus";
			}
			else {
				HttpSession session = request.getSession();
				session.setAttribute("dstatus", "Parking was rejected");
				
				return "depaturestatus";
			}	
		}
		else {
			HttpSession session = request.getSession();
			session.setAttribute("dstatus", "Enter valid vehicle number");
			
			return "depaturestatus";
		}
		
	}
	
	
	
}
