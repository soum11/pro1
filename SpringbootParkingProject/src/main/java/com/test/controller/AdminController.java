package com.test.controller;


import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.test.entities.Admin;
import com.test.entities.Userentry;
import com.test.service.AdminService;
import com.test.service.UserService;
import com.test.impl.AdminDaoImpl;

@Controller
public class AdminController {
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private UserService userService;

	@RequestMapping("/adminadd")
	public String adminpage(Model model, HttpServletRequest request, Admin admn) {
		HttpSession session = request.getSession();
		session.setAttribute("yoy", null);
		model.addAttribute("admin", admn);
		return "adminadddatafile";
	}
	
	
	
	
	
	@RequestMapping(value = "/adminaddsub", method = RequestMethod.POST)
	public String adminadddata(@Valid @ModelAttribute("admin") Admin usr, @RequestParam("secpin") String secupin, HttpServletRequest request, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			return "adminadddatafile";
		}
		if(secupin.equals("ABCD1234")) {
			if(adminService.admintest(usr.getAdmin_username())) {
				HttpSession session = request.getSession();
				session.setAttribute("yoy", "ADMIN USERNAME ALREADY EXISTS");
				return "adminadddatafile";
			}
			else {
				adminService.add(usr);
				HttpSession session = request.getSession();
				session.setAttribute("yoy", "ADMIN SUCCESSFULLY ADDEDD");
				return "adminadddatafile";
			}
		}
		
		HttpSession session = request.getSession();
		session.setAttribute("yoy", "SECURITY PIN IS NOT CORRECT");
		
		return "adminadddatafile";
	}
	
	@RequestMapping("/adminlog")
	public String userpage(Model model, HttpServletRequest request, Admin admn) {
		HttpSession session = request.getSession();
		session.setAttribute("zoz", null);
		model.addAttribute("adminlog", admn);
		return "adminlogin";
	}
	
	@RequestMapping(value = "/adminloginsub", method = RequestMethod.POST)
	public String admindataval(@Valid @ModelAttribute("adminlog") Admin usr, HttpServletRequest request, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			return "adminlogin";
		}
		if(adminService.adminloginval(usr)) {
			
			List<Userentry> data = userService.getallvechdets();
			
			System.out.println(data.size());
			
			
			
			HttpSession session = request.getSession();
			session.setAttribute("alldata", data);
			session.setAttribute("non", null);
			
			
			
			return "adminrequests";
		}
		
		HttpSession session = request.getSession();
		session.setAttribute("zoz", "INCORRECT USERNAME AND PASSWORD");
		return "adminlogin";
	}
	
	
	@RequestMapping("/acceptpark")
	public String acceptparking(HttpServletRequest request){
		
		String tknum = request.getParameter("tnum");
		String floor = request.getParameter("floor");
		String vnum = request.getParameter("vnum");
		
		System.out.println(tknum+" "+floor+" "+vnum);
		
		String token = floor + "-" + tknum;
		boolean y = userService.iftoken(vnum);
		if(y) {
			boolean x = userService.parktokencheck(token);
			if(x) {
				HttpSession session = request.getSession();
				session.setAttribute("non", "TOKEN NOT AVAILABLE");
				session.setAttribute("token", vnum);
				return "adminrequests";
			}
			else {
				userService.addtokennum(token, vnum);
				
				
				HttpSession session = request.getSession();
				session.setAttribute("non", null);
				List<Userentry> data = userService.getallvechdets();
				
				System.out.println(data.size());
				
				HttpSession session1 = request.getSession();
				session.setAttribute("alldata", data);
				
				
				
				return "adminrequests";
			}
		}
		
		return "adminrequests";
		
	}
	
	@RequestMapping("/rejectpark")
	public String rejectparking(HttpServletRequest request) {
		
		String vnum = request.getParameter("vnum");
		System.out.println(vnum);
		
		userService.rejectparking(vnum);
		List<Userentry> data = userService.getallvechdets();
		
		System.out.println(data.size());
		
		HttpSession session = request.getSession();
		session.setAttribute("alldata", data);
		
		return "adminrequests";
	}
	
	
	@RequestMapping("/adminpark")
	public String adminlogin(HttpServletRequest request) {
		List<Userentry> data = userService.getallvechdets();
		
		HttpSession session = request.getSession();
		session.setAttribute("alldata", data);
		
		
		session.setAttribute("non", null);
		
		
		return "adminrequests";
	}
	
	
	@RequestMapping("/admindepreq")
	public String admindeprequ(HttpServletRequest request) {
		List<Userentry> data = userService.getallvechdets();
		HttpSession session = request.getSession();
		session.setAttribute("alldata", data);
		
		return "admindeprequest";
	}
	
	@RequestMapping("/acceptdep")
	public String acceptdepart(HttpServletRequest request) {
		
		String vnum = request.getParameter("vnum");
		
		userService.acceptdeparture(vnum);
		List<Userentry> data = userService.getallvechdets();
		HttpSession session = request.getSession();
		session.setAttribute("alldata", data);
		
		return "admindeprequest";
	}
	
	
	@RequestMapping("/rejectdep")
	public String rejectdepart(HttpServletRequest request) {
		
        String vnum = request.getParameter("vnum");
		
		userService.rejectdeparture(vnum);
		
		return "admindeprequest";
	}
	
	@RequestMapping("/currentp")
	public String curretpar(HttpServletRequest request) {
		
		List<Userentry> current = userService.getallCurrent();
		HttpSession session = request.getSession();
		session.setAttribute("current", current);
		
		return "adminallparked";
	}
	
	@RequestMapping("/enterpark")
	public String enteredparking(HttpServletRequest request) {
		
		List<Userentry> enter = userService.getallEntered();
		HttpSession session = request.getSession();
		session.setAttribute("enter", enter);
		
		return "adminenteredpark";
	}
	
	@RequestMapping("/allparkleft")
	public String alldepart(HttpServletRequest request) {
		
		List<Userentry> left = userService.getallLeft();
		HttpSession session = request.getSession();
		session.setAttribute("left", left);
		
		return "adminalldeparted";
	}
	
	@RequestMapping("/parklot")
	public String parkingLot(HttpServletRequest request) {
		
		List<String> tokens = userService.getalltokens();
		HttpSession session = request.getSession();
		session.setAttribute("tokens", tokens);
		
		
		return "parkinglot";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
