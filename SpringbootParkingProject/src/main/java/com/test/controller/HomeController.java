package com.test.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.test.entities.Admin;

@Controller
public class HomeController {
	
	@RequestMapping("/")
	public String homepage() {
		return "home";
	}
	
	@RequestMapping("/adminservice")
	public String adminpage(Model model, HttpServletRequest request, Admin admn) {
		HttpSession session = request.getSession();
		session.setAttribute("zoz", null);
		model.addAttribute("adminlog", admn);
		return "adminlogin";
	}
	
	@RequestMapping("/userpagedets")
	public String userpage() {
		return "userservices";
	}
	
	
	
	
	
}
