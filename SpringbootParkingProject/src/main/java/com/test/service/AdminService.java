package com.test.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.dao.AdminDao;
import com.test.entities.Admin;
import com.test.entities.Userentry;

@Service
@Transactional
public class AdminService {
	
	@Autowired
	private AdminDao admindao;
	
    public AdminService() {
    	
    }
    
    public Admin add(Admin user) {
        if (user == null) {
            throw new IllegalArgumentException("The passed object cannot be null.");
        }
        admindao.add(user);
        
        return user;
    }
	
    public boolean admintest(String username) {
    	
    	List<Admin> result = admindao.getall();
    	for(Admin obj : result) {
    		if(username.equals(obj.getAdmin_username())) {
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    public boolean adminloginval(Admin user) {
    	
    	List<Admin> result = admindao.getall();
    	for(Admin obj : result) {
    		if(user.getAdmin_username().equals(obj.getAdmin_username()) && user.getPassword().equals(obj.getPassword())) {
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    
    

	
	
}
