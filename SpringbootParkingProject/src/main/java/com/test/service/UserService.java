package com.test.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.dao.UserDao;
import com.test.entities.Userentry;

@Service
@Transactional
public class UserService {
	
	@Autowired
    private UserDao userDao;

    public UserService() {
    	
    }
    
    public void add(Userentry user) {
        if (user == null) {
            throw new IllegalArgumentException("The passed object cannot be null.");
        }
        userDao.add(user);
    }
    
    public String vehicletestpp(String vehiclenumber) {
    	
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(obj.getVehiclenumber().equals(vehiclenumber)) {
    			if(obj.getCurrentstatus() != null) {
    				if(obj.getCurrentstatus().equals("PARKED")) {
    					return "THE REGISTRATION/VEHICLE NUMBER ALREADY EXISTS IN THE PARKING LOT";
    				}
    				return null;
    			}
    			return "THE REGISTRATION/VEHICLE NUMBER ALREADY WAITING PARKING";
    		}
    	}
    	
    	return null;
    }
	
    public boolean vehicletest(String vehiclenumber) {
    	
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(obj.getVehiclenumber().equals(vehiclenumber)) {
    			if(obj.getCurrentstatus() != null) {
    				if(obj.getCurrentstatus().equals("PARKED")) {
    					return true;
    				}  				
    			}
    			else {
    				return true;
    			}
    		}
    	}
    	
    	return false;
    }
    
    public boolean vehicleteststatus(String vehiclenumber) {
    	
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(obj.getVehiclenumber().equals(vehiclenumber)) {
    			if(obj.getCurrentstatus() != null) {
    				if(obj.getParkingoperator() != null) {
    					return true;
    				}  				
    			}
    			else {
    				return true;
    			}
    		}
    	}
    	
    	return false;
    }
    
  
    public List<Userentry> getallvechdets(){
    	List<Userentry> result = userDao.findal();
    	return result;
    }
    
    public boolean parktokencheck(String tokennum) {
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(tokennum.equals(obj.getTokennumber()) && obj.getCurrentstatus().equals("PARKED")) {
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    public boolean iftoken(String vehiclenum) {
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(vehiclenum.equals(obj.getVehiclenumber())) {
    			
    			if(obj.getTokennumber() != null) {
    				return false;
    			}
    			else {
    				return true;
    			}
    			
    		}
    	}
    	
    	return false;
    }
    
    public void addtokennum(String tokennum, String vehiclenum) {
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(vehiclenum.equals(obj.getVehiclenumber())) {
    			obj.setTokennumber(tokennum);
    			obj.setRequeststatus("ACCEPTED");
    			obj.setCurrentstatus("PARKED");
    			
    			LocalDate datenow = LocalDate.now();
    			String daten = datenow.toString();
    			obj.setIndate(daten);
    			
    			LocalTime timenow = LocalTime.now();
    			String timen = timenow.toString();
    			obj.setIntime(timen);
    			
    			userDao.update(obj);
    		}
    	}
    }
    
    public void rejectparking(String vehiclenum) {
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(vehiclenum.equals(obj.getVehiclenumber())) {
    			obj.setRequeststatus("REJECTED");
    			userDao.update(obj);
    		}
    	}
    }
	
    public String passtoken(String vehiclenum) {
    	List<Userentry> result = userDao.findal();
    	String res=null ;
    	for(Userentry obj : result) {
    		System.out.println(obj.getVehiclenumber()+"  ,  "+vehiclenum);
    		
    		if(vehiclenum.equals(obj.getVehiclenumber())) {
    			if(obj.getTokennumber() != null) {
    				if(obj.getCurrentstatus() != null) {
    					if(obj.getCurrentstatus().equals("PARKED")) {
        					return "You token nuber is -> " + obj.getTokennumber();
        				}
        				else {
        					return "You have already left and your token was " + obj.getTokennumber();
        					
        				}
    				}
    			}
    			else {
    				if(obj.getRequeststatus().equals("PENDING")) {
    					return "Your request still under process, please wait for a while";
    				}
    				else {
    					return "Parking was not available";
    				}
    			}
    		}
    		
    	}
    	
    	return "Enter Valid Vehicle number";
    }
    
    public boolean tokenexists(String vehiclenum) {
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(vehiclenum.equals(obj.getVehiclenumber())) {
    			if(obj.getTokennumber() != null) {
    				return true;
    			}
    		}
    	}
    	
    	return false;
    }
    
    public boolean parkstatus(String vehiclenum) {
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(vehiclenum.equals(obj.getVehiclenumber())) {
    			if(obj.getCurrentstatus().equals("LEFT")) {
    				return true;
    			}
    		}
    	}
    	
    	return false;
    }
    
    public String setamount(String vehiclenum) {
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(vehiclenum.equals(obj.getVehiclenumber())) {
    			if(obj.getVehicletype().equals("Two-Wheeler")) {
    				return "You have to Pay Rs.75";
    			}
    			else {
    				return "You have to Pay Rs.100";
    			}
    		}
    	}
    	
    	return "Enter valid vehicle number";
    }
    
    
    public void reqaccept(String vehiclenum, String amount) {
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(vehiclenum.equals(obj.getVehiclenumber())) {
    			if(obj.getVehiclenumber().equals(vehiclenum)) {
    				obj.setAmount(amount);
    				obj.setParkingoperator("YES");
    			}
    			else {
    			}
    		}
    	}
    }
    
    
    public String departstat(String vehiclenum) {
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(vehiclenum.equals(obj.getVehiclenumber())) {
    			if(obj.getParkingoperator().equals("YES")) {
    				return "Request under process";
    			}
    			if(obj.getParkingoperator().equals("ACCEPTED")) {
    				return "Request accepted, You can leave now, Do visit again";
    			}
    			if(obj.getParkingoperator().equals("REJECTED")) {
    				return "Request has been denied please check how to be paid";
    			}
    		}
    	}
    	
    	return "Enter valid vehicle number";
    }
    
    
    public void acceptdeparture(String vehiclenum) {
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(vehiclenum.equals(obj.getVehiclenumber())) {
    			obj.setParkingoperator("ACCEPTED");
    			obj.setPaymentstatus("DONE");
    			obj.setCurrentstatus("LEFT");
    			LocalDate datenow = LocalDate.now();
    			String daten = datenow.toString();
    			obj.setOutdate(daten);
    			
    			LocalTime timenow = LocalTime.now();
    			String timen = timenow.toString();
    			obj.setOuttime(timen);
    		}
    	}
    }
    
    public void rejectdeparture(String vehiclenum) {
    	List<Userentry> result = userDao.findal();
    	for(Userentry obj : result) {
    		if(vehiclenum.equals(obj.getVehiclenumber())) {
    			obj.setParkingoperator("REJECTED");
    		}
    	}
    }
    
    public List<Userentry> getallLeft(){
    	List<Userentry> result = userDao.findal();
    	List<Userentry> res = new ArrayList<Userentry>();
    	for(Userentry obj : result) {
    		if(obj.getCurrentstatus() != null){
    			if(obj.getCurrentstatus().equals("LEFT")) {
    				res.add(obj);
    			}
    		}
    	}
    	return res;
    }
    
    
    public List<Userentry> getallCurrent(){
    	List<Userentry> result = userDao.findal();
    	List<Userentry> res = new ArrayList<Userentry>();
    	for(Userentry obj : result) {
    		if(obj.getCurrentstatus() != null){
    			if(obj.getCurrentstatus().equals("PARKED")) {
    				res.add(obj);
    			}
    		}
    	}
    	return res;
    }
    
    
    public List<Userentry> getallEntered(){
    	List<Userentry> result = userDao.findal();
    	List<Userentry> res = new ArrayList<Userentry>();
    	for(Userentry obj : result) {
    		if(obj.getCurrentstatus() != null){
    			res.add(obj);
    		}
    	}
    	return res;
    }
    
    
    public List<String> getalltokens(){
    	List<Userentry> result = userDao.findal();
    	List<String> res = new ArrayList<>();
    	for(Userentry obj : result) {
    		if(obj.getCurrentstatus() != null){
    			if(obj.getCurrentstatus().equals("PARKED")) {
    				res.add(obj.getTokennumber());
    			}
    		}
    	}
    	return res;
    }

	public Userentry findById(int vehicleid) {
		Userentry obj = userDao.findById(vehicleid);
		return obj;
	}
    
}
